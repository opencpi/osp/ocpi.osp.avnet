.. avnet top level project documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.

.. _avnet-top-level:

.. |trade| unicode:: U+2122
   :ltrim:

.. |reg| unicode:: U+00AE
   :ltrim:


Avnet OSP
=========

OpenCPI System Support Package (OSP) that contains assets that support the
Avnet\ |reg| MicroZed\ |trade| and PicoZed\ |trade| families of System On Module (SOM) and FPGA Mezzanine Card (FMC) expansion/carrier cards. 

Supported systems and their associated OpenCPI HDL platform names include:

* MicroZed 7Z010 SOM and Microzed Carrier Card (``microzed_10_cc``)

* MicroZed 7Z020 SOM and Microzed Carrier Card (``microzed_20_cc``)

* PicoZed 7Z010 SOM and PicoZed Carrier Card (``picozed_10_cc``)

* PicoZed 7Z015 SOM and PicoZed Carrier Card (``picozed_15_cc``)

* PicoZed 7Z020 SOM and PicoZed Carrier Card (``picozed_20_cc``)

* PicoZed 7Z030 SOM and PicoZed Carrier Card (``picozed_30_cc``)

The OSP provides a user "getting started guide" for each system to be used when
setting up the system hardware for OpenCPI and when setting up the system
for OpenCPI using the *OpenCPI Installation Guide*.

The OSP also provides two case study guides, one for MicroZed systems and one for PicoZed systems,
that describe the OSP development and test process followed for these platforms. 

.. note::
   
   ``hdl/assemblies/`` contains copies of assemblies from the OpenCPI assets project, with the
   addition of MicroZed SOM- and PicoZed SOM-specific container XML files.
   A desirable framework improvement would be to mitigate the need to copy assemblies from external
   projects in order to use containers made possible internally.


.. toctree::
   :maxdepth: 2
   :glob:
      
   hdl/platforms/gsg
   
..   components/components
..   hdl/primitives/primitives
..   specs/specs
