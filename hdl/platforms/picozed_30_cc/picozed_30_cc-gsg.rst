.. picozed_30_cc Getting Started Guide Documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.

.. _picozed_30_cc-gsg:

.. |trade| unicode:: U+2122
   :ltrim:

.. |reg| unicode:: U+00AE
   :ltrim:


OpenCPI Avnet PicoZed 7Z030 Getting Started Guide
=================================================

.. ocpi_documentation_include:: ../include/picozed_cc_content/picozed_overview.inc
				
   |device_name|: 7Z030
   |platform_name|: picozed_30_cc
   |path_to_figures|: ../include/picozed_cc_figures/

.. ocpi_documentation_include:: ../include/picozed_cc_content/picozed_som_setup.inc

   |device_name|: 7Z030
   |platform_name|: picozed_30_cc
   |path_to_figures|: ../include/picozed_cc_figures/

See the
`PicoZed 7Z015/7Z030 SOM Hardware User Guide <https://www.avnet.com/opasdata/d120001/medias/docus/126/$v2/5279-UG-PicoZed-7015-7030-V2_0.pdf>`_
for details on SOM switches, jumpers, and configuration.

Setting up the PZCC
~~~~~~~~~~~~~~~~~~~

.. note::

   These configuration steps are also shown in step 4 on the `Quick Start instruction card <https://www.avnet.com/opasdata/d120001/medias/docus/136/AES-PZCC-FMC-V2-G-QSC-v2b.pdf>`_.

   To set up the PZCC for operation with the PicoZed 7Z030, leave all PZCC jumpers at their default settings.
   In particular, the VADJ select jumper (JP5) on the PZCC *must* be configured for 1.8V
   as specified in the section "VADJ selection-JP5" in
   the `Avnet PZCC Hardware User Guide <https://www.avnet.com/opasdata/d120001/medias/docus/136/AES-PZCC-FMC-V2-G-ug.pdf>`_
   and as indicated by the silkscreen warning on the PZCC.

.. _pzcc-jumper-diagram:

.. figure:: ../include/picozed_cc_figures/picozed_cc_jmp.png
   :alt: 
   :align: center

   Avnet PZCC: Top View of FMC GA Address and VADJ Jumpers for the FMC LPC

Installing the SOM on the PZCC
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In order for the PicoZed SOM to be powered on, it must be installed on the PZCC via the JX1, JX2, and JX3
SOM interface MicroHeaders.  See the `PZCC Hardware User Guide <https://www.avnet.com/opasdata/d120001/medias/docus/136/AES-PZCC-FMC-V2-G-ug.pdf>`_ for details about these connectors.

.. ocpi_documentation_include:: ../include/picozed_cc_content/picozed_dc_setup.inc

   |device_name|: 7Z030
   |platform_name|: picozed_30_cc
   |path_to_figures|: ../include/picozed_cc_figures/

.. ocpi_documentation_include:: ../include/picozed_cc_content/picozed_sfw_setup.inc
				
   |device_name|: 7Z030
   |platform_name|: picozed_30_cc
   |prompt_name|: 7030
   |path_to_figures|: ../include/picozed_cc_figures/
