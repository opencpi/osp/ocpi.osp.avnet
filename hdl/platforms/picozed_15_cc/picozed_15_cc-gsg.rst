.. picozed_15_cc Getting Started Guide Documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.

.. _picozed_15_cc-gsg:

.. |trade| unicode:: U+2122
   :ltrim:

.. |reg| unicode:: U+00AE
   :ltrim:


OpenCPI Avnet PicoZed 7Z015 Getting Started Guide
=================================================
.. ocpi_documentation_include:: ../include/picozed_cc_content/picozed_overview.inc
				
   |device_name|: 7Z015
   |platform_name|: picozed_15_cc
   |path_to_figures|: ../include/picozed_cc_figures/

.. ocpi_documentation_include:: ../include/picozed_cc_content/picozed_som_setup.inc

   |device_name|: 7Z015
   |platform_name|: picozed_15_cc
   |path_to_figures|: ../include/picozed_cc_figures/

See the `PicoZed 7Z015/7Z030 SOM Hardware User Guide <https://www.avnet.com/opasdata/d120001/medias/docus/126/$v2/5279-UG-PicoZed-7015-7030-V2_0.pdf>`_
for details on SOM switches, jumpers, and configuration.

.. ocpi_documentation_include:: ../include/picozed_cc_content/pzcc_setup.inc

   |device_name|: 7Z015
   |platform_name|: picozed_15_cc
   |path_to_figures|: ../include/picozed_cc_figures/

.. ocpi_documentation_include:: ../include/picozed_cc_content/picozed_dc_setup.inc

   |device_name|: 7Z015
   |platform_name|: picozed_15_cc
   |path_to_figures|: ../include/picozed_cc_figures/

.. ocpi_documentation_include:: ../include/picozed_cc_content/picozed_sfw_setup.inc
				
   |device_name|: 7Z015
   |platform_name|: picozed_15_cc
   |prompt_name|: 7015
   |path_to_figures|: ../include/picozed_cc_figures/


