.. Primitive directory index page


Getting Started Guides
======================
Available MicroZed and PicoZed getting started guides.

.. toctree::
   :maxdepth: 2
   :glob:

   */*-gsg
