.. microzed_20_cc Getting Started Guide Documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.

.. |trade| unicode:: U+2122
   :ltrim:

.. |reg| unicode:: U+00AE
   :ltrim:

.. _microzed_20_cc-gsg:

OpenCPI Avnet MicroZed 7Z020 Getting Started Guide
==================================================

.. ocpi_documentation_include:: ../include/microzed_cc_content/microzed_overview.inc
				
   |device_name|: 7Z020
   |platform_name|: microzed_20_cc
   |path_to_figures|: ../include/microzed_cc_figures/

.. ocpi_documentation_include:: ../include/microzed_cc_content/microzed_som_setup.inc

   |device_name|: 7Z020
   |platform_name|: microzed_20_cc
   |path_to_figures|: ../include/microzed_cc_figures/

.. ocpi_documentation_include:: ../include/microzed_cc_content/mzcc_setup.inc

   |device_name|: 7Z020
   |platform_name|: microzed_20_cc
   |path_to_figures|: ../include/microzed_cc_figures/
   
.. ocpi_documentation_include:: ../include/microzed_cc_content/microzed_dc_setup.inc

   |device_name|: 7Z020
   |platform_name|: microzed_20_cc
   |path_to_figures|: ../include/microzed_cc_figures/

.. ocpi_documentation_include:: ../include/microzed_cc_content/microzed_sfw_setup.inc
				
   |device_name|: 7Z020
   |platform_name|: microzed_20_cc
   |prompt_name|: 7020
   |path_to_figures|: ../include/microzed_cc_figures/

