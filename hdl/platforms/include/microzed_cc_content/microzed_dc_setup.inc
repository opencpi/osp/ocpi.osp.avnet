.. Shared file for connecting FMC Daughtercards

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.

Connecting FMC Daughtercards (Optional)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The MZCC has an FMC LPC slot that can be used to connect plug-in modules or daughtercards,
as shown in :numref:`|platform_name|-mzcc-fmc-diagram`.
OpenCPI currently supports two daughtercards that can be installed on the MZCC:

* `Analog Devices FMCOMMS2 <https://www.analog.com/en/design-center/evaluation-hardware-and-software/evaluation-boards-kits/eval-ad-fmcomms2.html#eb-overview>`_

* `Analog Devices FMCOMMS3 <https://www.analog.com/en/design-center/evaluation-hardware-and-software/evaluation-boards-kits/eval-ad-fmcomms3-ebz.html#eb-overview>`_

.. _|platform_name|-mzcc-fmc-diagram:

.. figure:: |path_to_figures|microzed_fmc.png
   :alt: 
   :align: center

   Avnet MZCC: FMC-LPC Slot
