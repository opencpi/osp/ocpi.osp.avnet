.. Shared text for MicroZed SOM setup.

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.

.. _|platform_name|-hw-setup:

Setting up the MicroZed Hardware
--------------------------------

This section describes how to set up the boards in a MicroZed embedded system for OpenCPI.
The locations of the target jumpers on the MicroZed |device_name| SOM and MZCC are highlighted
in :numref:`|platform_name|-jumper-diagram`

.. _|platform_name|-jumper-diagram:

.. figure:: |path_to_figures|microzed_top.png
   :alt: 
   :align: center

   MicroZed System: Top View with SOM and MZCC Jumpers

Perform the steps described in the following sections *before* applying power to the boards.

Configuring the MicroZed |device_name| to Boot from an SD Card
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

By default, the boot mode switches on the MicroZed |device_name| are
set to boot from the Quad Serial Peripheral Interface (QSPI).
Set these switches (J1, J2, J3) to the positions shown in :numref:`|platform_name|-som-jumper-diagram`
so that the |device_name| will boot from the SD card. See the section "Configuration Modes" in the
`MicroZed Hardware User Guide <https://www.avnet.com/opasdata/d120001/medias/docus/178/UG-HW-AES-Z7MB-7Z010-SOM-G-v6.pdf>`_
for detailed information.

.. _|platform_name|-som-jumper-diagram:

.. figure:: |path_to_figures|microzed_som_boot_jmp.jpg
   :alt: 
   :align: center

   Avnet MicroZed |device_name|: Top View with SD Card Boot Mode Switch Settings

