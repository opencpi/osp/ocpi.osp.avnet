.. Common MZCC setup text for all MicroZed systems.

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.

Setting up the MZCC
~~~~~~~~~~~~~~~~~~~

By default, the VADJ jumper (J6) is set to 1.8V but can be set to 2.5V or 3.3V.
Adjust the VADJ jumper to the desired VADJ level as necessary.
:numref:`|platform_name|-mzcc-jumper-diagram` shows the jumper set to 2.5V.
See the section "Jumpers, configuration and test points" in the
`Avnet MZCC User Guide <https://www.avnet.com/opasdata/d120001/medias/docus/58/AES-MBCC-FMC-G-MBCC_FMC_UG_1.1.pdf>`_
for details.


.. _|platform_name|-mzcc-jumper-diagram:

.. figure:: |path_to_figures|microzed_cc_jmp.jpg
   :alt: 
   :align: center

   Avnet MZCC: Top View of VADJ Jumper Setting to V2.5 for the FMC LPC

Installing the SOM on the MZCC
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In the OpenCPI MicroZed system, the MicroZed SOM gets its power
from the MZCC.  For the MicroZed SOM to be powered on, it must be installed on the MZCC via the JX1 and JX2
SOM interface MicroHeaders.  See the section "JX1 and JX2 MicroZed interface microheaders" in the
`MZCC User Guide <https://www.avnet.com/opasdata/d120001/medias/docus/58/AES-MBCC-FMC-G-MBCC_FMC_UG_1.1.pdf>`_ 
for information about these connectors.
