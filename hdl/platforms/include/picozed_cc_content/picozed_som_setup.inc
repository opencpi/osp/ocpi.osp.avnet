.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.

.. _|platform_name|-hw-setup:

Setting up the PicoZed Hardware
-------------------------------

This section describes how to set up the boards in a PicoZed embedded system for OpenCPI.
The locations of the target jumpers on the PicoZed |device_name| SOM and PZCC are highlighted
in :numref:`|platform_name|-jumper-diagram`

.. _|platform_name|-jumper-diagram:

.. figure:: |path_to_figures|picozed_top.jpg
   :alt: 
   :align: center

   PicoZed: Top View with |device_name| SOM and PZCC Jumpers

Perform the steps described in the following sections *before* applying power to the boards.

Configuring the PicoZed |device_name| to Boot from an SD Card
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

By default, the boot mode switches on the PicoZed |device_name| are
set to boot from the Quad Serial Peripheral Interface (QSPI).
Set these switches (JT4, SW1) to HIGH so that the |device_name| will boot from the SD card,
as shown in :numref:`|platform_name|-som-jumper-diagram`

.. _|platform_name|-som-jumper-diagram:

.. figure:: |path_to_figures|picozed_som_boot_jmp.png
   :alt: 
   :align: center

   PicoZed |device_name|: Top View with SD Card Boot Mode Switch Settings

