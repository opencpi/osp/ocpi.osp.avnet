.. Common file for all PicoZed systems but 7Z030.

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.

Setting up the PZCC
~~~~~~~~~~~~~~~~~~~

Configure the PZCC jumpers as follows:

* Leave the FMC GA address select (JP4) at the default setting, where the FMC address is 00 and the jumpers are placed at locations 3-5 and 4-6.  See the section "FMC GA [1:0] jumper header" in the `Avnet PZCC Hardware User Guide <https://www.avnet.com/opasdata/d120001/medias/docus/136/AES-PZCC-FMC-V2-G-ug.pdf>`_ for details.

* Adjust the VADJ select (JP5) to the desired VADJ level as necessary.  By default, JP5 is set to 1.8V but can be set to 2.5V or 3.3V. See Section 3.6, "VADJ selection - JP5" in the `Avnet PZCC Hardware User Guide <https://www.avnet.com/opasdata/d120001/medias/docus/136/AES-PZCC-FMC-V2-G-ug.pdf>`_ for details.

.. _|platform_name|-pzcc-jumper-diagram:

.. figure:: |path_to_figures|picozed_cc_jmp.png
   :alt: 
   :align: center

   Avnet PZCC: Top View of FMC GA Address and VADJ Jumpers for the FMC LPC

Installing the SOM on the PZCC
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In order for the PicoZed SOM to be powered on, it must be installed on the PZCC via the JX1, JX2, and JX3
SOM interface MicroHeaders.  See the `PZCC Hardware User Guide <https://www.avnet.com/opasdata/d120001/medias/docus/136/AES-PZCC-FMC-V2-G-ug.pdf>`_ for details about these connectors.
