.. Common PicoZed file for OpenCPI setup instructions

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.

.. _|platform_name|-enable-ocpi:

Enabling OpenCPI Development for the PicoZed
--------------------------------------------

This section gives information about the PicoZed |device_name| to use when
following the instructions in the chapter
“Enabling OpenCPI Development for Embedded Systems” of the
`OpenCPI Installation Guide <https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_Installation_Guide.pdf>`_.

.. note::

   The instructions in the next sections assume that the basic OpenCPI installation for the development host has already been done.
   If it hasn't, follow the instructions in the chapter "Installing OpenCPI on Development Hosts" of the
   `OpenCPI Installation Guide <https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_Installation_Guide.pdf>`_
   *before* proceeding to follow the instructions in these sections.


Installation Steps for the PicoZed
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This section contains information to be applied to the tasks described
in the section "Installation Steps for Platforms" of the `OpenCPI Installation Guide <https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_Installation_Guide.pdf>`_.


Supported OpenCPI Platforms and Vendor Tools
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:numref:`|platform_name|-sfw-reqs` lists the OpenCPI software (RCC)
and hardware (HDL (FPGA) platforms used for the PicoZed
and the third-party/vendor tools on which they depend.
The OpenCPI HDL platform name for the PicoZed |device_name| is ``|platform_name|``.

.. I used ascii art for this table to be able to control line breaks in column text.
   
.. Need to find out how to turn off "no line wrap" in HTML renderer so that column text will wrap in csv-table and list-table.

.. _|platform_name|-sfw-reqs:

.. table:: Supported OpenCPI Platforms for the PicoZed |device_name| and their Dependencies
	   
   +------------------------+-----------------------+--------------------+--------------------------------------+
   | OpenCPI                + Description           + OpenCPI            + Required Third-Party/                |
   |                        +                       +                    +                                      |
   | Platform Name          +                       + Project/Repo       + Vendor Tools                         |
   +========================+=======================+====================+======================================+
   | ``xilinx19_2_aarch32`` + Xilinx\ |reg| Linux   + ``ocpi.core``      + Xilinx Vitis\ |trade| SDK 2019.2     |
   |                        +                       +                    +                                      |
   |                        + from 2019Q2 (2019.2)  + built-in           + Xilinx Binary Zynq Release 2019.2    |
   |                        +                       +                    +                                      |
   |                        + for Zynq\ |reg|-7000  +                    + Xilinx Linux ``git`` clone           |
   |                        +                       +                    +                                      |
   |                        +                       +                    + Xilinx Linux tag: ``xilinx-v2019.2`` |
   +------------------------+-----------------------+--------------------+--------------------------------------+
   | ``|platform_name|``      + Avnet PicoZed         + ``ocpi.osp.avnet`` + Xilinx Vivado\ |reg| WebPACK         |
   |                        +                       +                    +                                      |
   |                        + Zynq-7000 FPGA/PL     +                    +                                      |
   +------------------------+-----------------------+--------------------+--------------------------------------+
   
Notes about the OpenCPI platforms listed in the table:

* ``xilinx19_2_aarch32`` is the primary RCC platform for the PicoZed.

* The ``xilinx19_2_aarch32`` RCC platform is configured for DHCP.

.. _|platform_name|-vendor-tools:

Installing the Vendor Tools for the OpenCPI Platforms
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Follow the instructions in the section "Installing Xilinx Tools"
in the chapter “Installing Third-party/Vendor Tools” in the
`OpenCPI Installation Guide <https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_Installation_Guide.pdf>`_
to install the Xilinx tools listed as platform dependencies in :numref:`|platform_name|-sfw-reqs`

.. note::
   
   Perform this step first, *before* installing/building the OpenCPI platforms for the PicoZed.

.. note::

   The PicoZed system requires the Xilinx Binary Zynq Release 2019.2 (see :ref:`|platform_name|-sfw-reqs`).
   As a convenience, the OpenCPI Avnet OSP provides this package for the PicoZed |device_name| in
   the location ``<path-to-avnet-osp>/hdl/platform/|platform_name|/2019.2-|platform_name|-release.tar.xz``.
   This package can be placed in the ``/tools/Xilinx/ZynqReleases/2019.2/`` (or ``/opt/Xilinx/ZynqReleases/2019.2/``)
   directory instead of downloading it from the Xilinx website as described in the
   section "Xilinx Binary Releases for Zynq-7000 and Zynq-Ultrascale Systems" in the *OpenCPI Installation Guide*.

.. _|platform_name|-ocpiadmin-install:

Installing and Building the OpenCPI Platforms for the PicoZed
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
After installing the required vendor tools, follow the instructions
in the section “Installation Steps for Platforms” in the
`OpenCPI Installation Guide <https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_Installation_Guide.pdf>`_
to install and build the ``xilinx19_2_aarch32`` and ``|platform_name|`` platforms for the PicoZed system.

Both these platforms must be installed and built with ``ocpiadmin install platform``
before proceeding to set up the SD card for the PicoZed. For example:

.. code-block:: bash

   ocpiadmin install platform xilinx19_2_aarch32
   
   ocpiadmin --package-id=ocpi.osp.avnet install platform |platform_name|

.. note::

   Specifying the ``--package-id`` option directs the command to download and build the OSP.
   See the `ocpiadmin(1) man page <https://opencpi.gitlab.io/releases/latest/man/ocpiadmin.1.html>`_ for command usage details.

.. note::
   
   The ``xilinx19_2_aarch32`` platform is also used by other systems, such as the
   Avnet (Digilent) ZedBoard (in OpenCPI, the ``zed`` and ``zed_ise`` HDL platforms).
   If the ``xilinx19_2_aarch32`` platform has already been installed and built
   for another supported system, it can apply to the PicoZed and does not need to be installed and built.

Installation Steps for the PicoZed After Its Platforms Have Been Installed
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This section contains information to be applied to the tasks described
in the section "Installation Steps for
Embedded Systems After Their Platforms Have Been Installed" in the
`OpenCPI Installation Guide <https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_Installation_Guide.pdf>`_.

.. _|platform_name|-net-mode:

Connecting the PicoZed to a DHCP-Supported Network (Optional)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

OpenCPI allows three modes of operation on embedded systems: *server mode*, *network mode*, and *standalone mode*
(see the beginning paragraphs in the section "Installation Steps for Systems after their Platforms are Installed"
of the `OpenCPI Installation Guide <https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_Installation_Guide.pdf>`_ for details).

Using server mode or network mode for OpenCPI on the PicoZed embedded system requires
establishing an Ethernet connection from the PZCC to a network that supports DHCP.

There is one Ethernet port located on the PZCC,
as shown in :numref:`|platform_name|-picozed-eth-diagram`  Use a 1-Gigabit Ethernet cable to
connect this port to the DHCP-supported network.

.. _|platform_name|-picozed-eth-diagram:

.. figure:: |path_to_figures|picozed_ether.jpg
   :alt: 
   :align: center

   Avnet PZCC: Ethernet Port

.. _|platform_name|-sd-card-setup:

Setting up the SD Card for the PicoZed
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. note::

   The process of creating a bootable SD card for the PicoZed system requires an
   SD card reader/writer to be connected to the development host. If you have not
   done so already, follow the instructions in the section "Using SD Card Reader/Writer Devices" of the
   `OpenCPI Installation Guide <https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_Installation_Guide.pdf>`_
   to set up an SD card reader/writer before proceeding with this section.

The PicoZed boots from an SD card.  Enabling OpenCPI on
the PicoZed requires creating a bootable SD card that replaces
the factory SD card supplied with the PZCC kit.

Follow the instructions in the subsections "Preparing the SD Card Contents", "Writing the SD Card"
and "SD Card OpenCPI Startup Script Setup" in the
section "Installation Steps for Systems After Their Platforms are Installed" of the
`OpenCPI Installation Guide <https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_Installation_Guide.pdf>`_
to create a bootable SD card that enables the PicoZed for OpenCPI.

.. note::

   The recommended method described in the *OpenCPI Installation Guide* applies to the PicoZed: make a raw copy of the 
   factory-provided card to a new card, remove the factory-provided content (files and directories) from the raw copy, and then
   copy the OpenCPI-provided content to the raw copy. OpenCPI provides all of the necessary files for installation and
   deployment on the PicoZed.

Should you need to format an SD card for the PicoZed, it should be a single FAT32 partition.

The bootable SD card slot is located on the bottom side of the PZCC below the FMC LPC slot.

.. _|platform_name|-microSD-card-diagram:

.. figure:: |path_to_figures|picozed_fmc_sd.jpg
   :alt: 
   :align: center

   Avnet PZCC: MicroSD Card Slot

Once you have finished with this procedure, eject the SD card from the card reader/writer
on the development host and insert it into the PZCC SD card slot while the PZCC is powered off.

To eject an SD card, gently push it in and then release it.

.. _|platform_name|-serial-console:

Establishing a Serial Console Connection for the PicoZed
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Follow the instructions in the section "Establishing a Serial Console Connection"
in the `OpenCPI Installation Guide <https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_Installation_Guide.pdf>`_
to set up a serial console connection between the PicoZed and the development host. 

On the PicoZed system, the micro-USB serial port
is located on the top side of the PZCC (see :numref:`|platform_name|-serial-port-diagram`) and is labeled **UART**.
Connect the micro-USB to USB-A cable
from this port (J1) to the development host.

The PicoZed's serial USB port operates at 115200 baud.

.. _|platform_name|-serial-port-diagram:

.. figure:: |path_to_figures|picozed_uart.jpg
   :alt: 
   :align: center

   Avnet PZCC: Connected Serial USB Port

.. _|platform_name|-cfg-runtime:

Configuring the Runtime Environment on the PicoZed
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Follow the instructions in the section
"Configuring the Runtime Environment on the Embedded System"
in the `OpenCPI Installation Guide <https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_Installation_Guide.pdf>`_ to verify the runtime environment on the PicoZed embedded system before running any applications.

To boot the PicoZed:

* Ensure that:

  * The PicoZed SOM and PZCC boards are configured for OpenCPI, as described in :ref:`|platform_name|-hw-setup`.

  * The PicoZed PZCC and the development host are connected by a serial console cable, as described in :ref:`|platform_name|-serial-console`.

* Insert the prepared SD card into the SD card slot on the PZCC, as shown in :numref:`|platform_name|-microSD-card-diagram`

* Start a terminal emulator on the development host (usually with the ``screen`` command) at 115200 baud.

* Apply power to the PZCC.

* After Linux boots successfully, log in using ``root`` for user name and password (note that the password must be supplied twice when using server mode or network mode).

After a successful login, follow the mode-specific instructions in the section to establish the OpenCPI environment.
This step applies to network mode and standalone mode only. Server mode does not require the use of a setup script.

.. _|platform_name|-run-test:

Running the Test Application on the PicoZed
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The steps to build the platforms performed in :ref:`|platform_name|-ocpiadmin-install` result in a single executable FPGA
test application named ``bias`` that is ready to run. The ``bias`` test application is based on the
``testbias`` HDL assembly.  Both ``bias`` and ``testbias`` reside in the ``ocpi.assets`` built-in project
at ``applications/bias.xml`` and ``hdl/assemblies/testbias/`` respectively.

Follow the steps in the section "Running the Test Application" in the
`OpenCPI Installation Guide <https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_Installation_Guide.pdf>`_
that corresponds to the configured operation mode (server mode, network mode, or standalone mode) to run this application
to verify the installation and any HDL assemblies of the Avnet OSP itself.

