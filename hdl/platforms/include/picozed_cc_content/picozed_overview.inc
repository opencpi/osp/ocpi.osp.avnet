.. Common PicoZed system overview

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.

This document provides installation information that is specific
to the OpenCPI Avnet\ |reg| PicoZed\ |trade| embedded system when it includes
the ``|platform_name|`` OpenCPI HDL (FPGA) platform.  This platform
consists of the Avnet PicoZed |device_name| System On Module (SOM) and the
Avnet PicoZed FMC (FPGA Mezzanine Card) Carrier Card V2 (PZCC)
expansion board on which the |device_name| is required to be installed.

Use this document when configuring the PicoZed hardware for OpenCPI and
when performing the tasks described
in the chapter "Enabling OpenCPI Development for Embedded Systems"
in the 
`OpenCPI Installation Guide <https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_Installation_Guide.pdf>`_.
This document supplies details about enabling OpenCPI development on the PicoZed |device_name|
that can be applied to the procedures described in the referenced *OpenCPI Installation Guide* chapter.
The recommended method is to have the *OpenCPI Installation Guide* and this document
open in separate windows and refer to this document for any platform-specific details
while following the OpenCPI setup tasks described in the Installation Guide.

The following documents can also be used as references for the tasks described in this document:

* `OpenCPI User Guide <https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_User_Guide.pdf>`_
  
* `OpenCPI Glossary <https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_Glossary.pdf>`_

Note that the *OpenCPI Glossary* is also contained in both the *OpenCPI Installation Guide* and the
*OpenCPI User Guide*.

This document assumes a basic understanding of the Linux command line (or "shell") environment.

Document Revision History
-------------------------

.. _|platform_name|-rev-history:

.. csv-table:: OpenCPI |platform_name| Getting Started Guide: Revision History
   :header: "OpenCPI Version", "Description of Change", "Date"
   :widths: 10,30,10
   :class: tight-table

   "v2.1", "Initial Release", "4/2021"
   "v2.4", "Convert to RST, update for release", "5/2022"

Overview
--------

The Avnet PicoZed |device_name| is a member of a family of pin-compatible SOMs based on the
Xilinx\ |reg|  Zynq\ |reg|-7000 All Programmable (AP) SoC.
See the `Avnet PicoZed product brief <https://www.avnet.com/wps/wcm/connect/onesite/bd9e3b67-7af5-43b2-98a5-faefeb310808/5423-PB-AES-Z7PZ-SOM-G-V1-v2b.pdf?MOD=AJPERES&CACHEID=ROOTWORKSPACE.Z18_NA5A1I41L0ICD0ABNDMDDG0000-bd9e3b67-7af5-43b2-98a5-faefeb310808-nSCkRWT>`_ for information about the PicoZed board family and its variants.

The OpenCPI PicoZed system is designed to support FMC daughtercards.
To achieve this support, the OpenCPI PicoZed system requires the |device_name| SOM to be installed on an
Avnet PZCC, which provides an FMC low pin count (LPC) connector slot for connecting FMC daughtercards.
The PZCC powers, connects, and operates the PicoZed |device_name|.

OpenCPI has been tested on the PicoZed |device_name| with the Avnet PicoZed FMC Carrier Card V2.
See the `PZCC product brief <https://www.avnet.com/wps/portal/silica/products/product-highlights/2016/xilinx-picozed-fmc-carrier-card-v2/>`_ for information about this carrier card. In OpenCPI, the |device_name| SOM-PZCC configuration is called the ``|platform_name|`` HDL platform.

.. note::

   PZCC features that are additional to the FMC LPC slot, such as Digilent Pmods\ |trade|,
   pushbuttons, and other peripheral interfaces, are not currently supported for use with OpenCPI.

Installation Prerequisites
--------------------------

The following items are required for OpenCPI PicoZed system installation and setup:

* Avnet PicoZed |device_name|

* Avnet PZCC

* 12V @5A power supply for the PZCC

* micro-USB to USB-A cable

* micro-USB to female-USB adapter

* microSD card. Use the microSD card that comes with the product kit or provide a different microSD card. OpenCPI PicoZed setup has been tested using a 16GB microSD card.

These items are typically provided with the PicoZed |device_name| and/or PZCC product kits.

.. note::

   Ignore any quick start cards or instructions that come with the product kit.
   This getting started guide and the
   `OpenCPI Installation Guide <https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_Installation_Guide.pdf>`_
   provide the necessary instructions for setting up the PicooZed system for OpenCPI.

The following items are optional:

* 1-Gigabit Ethernet cable

* Analog Devices FMCOMMS2 or FMCOMMS3 daughtercard

Installation Summary
--------------------

To set up the PicoZed system for OpenCPI development, perform the following steps:

* Configure the PicoZed SOM and PZCC expansion board for use with OpenCPI and connect the two boards.  See :ref:`|platform_name|-hw-setup`.

* (Optional) Connect an FMCOMMS2 or FMCOMMS3 daughtercard to the PZCC.  See :ref:`|platform_name|-hw-setup`.

* Install OpenCPI on the development host.  See :ref:`|platform_name|-enable-ocpi`.

* Prepare the development host to support the PicoZed system:

  * Connect a serial cable from the development host to the Picozed.  See :ref:`|platform_name|-serial-console`.
    
  * Connect an SD card reader/writer to the development host. See :ref:`|platform_name|-sd-card-setup`.

  * (Optional) Connect the PicoZed system to a DHCP-supported network.  See :ref:`|platform_name|-net-mode`.

* Install the required vendor tools.  See :ref:`|platform_name|-vendor-tools`.

* Build the software and hardware platforms for the PicoZed system.  See :ref:`|platform_name|-ocpiadmin-install`.

* Prepare and write the bootable SD card for the PicoZed system.  See :ref:`|platform_name|-sd-card-setup`.

* Boot the PicoZed system and establish the OpenCPI environment on the system.  See :ref:`|platform_name|-cfg-runtime`.

* Run the OpenCPI-provided test application to confirm successful installation.  See :ref:`|platform_name|-run-test`.

