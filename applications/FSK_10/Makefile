# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License along
# with this program. If not, see <http://www.gnu.org/licenses/>.

$(if $(realpath $(OCPI_CDK_DIR)),,\
  $(error The OCPI_CDK_DIR environment variable is not set correctly.))
# This is the application Makefile for the "FSK" application
# If there is a FSK.cc (or FSK.cxx) file, it will be assumed to be a C++ main program to build and run
# If there is a FSK.xml file, it will be assumed to be an XML app that can be run with ocpirun.
# The RunArgs variable can be set to a standard set of arguments to use when executing either.

include $(OCPI_CDK_DIR)/include/application.mk

.SILENT: show
.PHONY: show

all: tap_gen

idata/tx_rrcos_taps.dat idata/rx_rrcos_taps.dat: tap_gen

tap_gen:
	$(eval numTaps=128)
	$(eval alpha=0.95)
	$(eval baudRate=6400)
	$(eval spb=39)
	$(eval maxTap=4096)
	# Remove old test data files
	rm -rf idata/*.dat idata/*.bin odata/*
	# Create test input data
	./scripts/gen_rrcos_taps.py $(numTaps) $(alpha) `echo "1/$(baudRate)" | bc -l` `echo "$(baudRate)*$(spb)" | bc -l` $(maxTap) idata/tx_rrcos_taps.dat
	./scripts/gen_rrcos_taps.py $(numTaps) $(alpha) `echo "1/$(baudRate)" | bc -l` `echo "$(baudRate)*$(spb)" | bc -l` $(maxTap) idata/rx_rrcos_taps.dat

clean::
	rm -rf idata/*.dat idata/*.bin odata/* scripts/*.pyc
