/*
 * This file is protected by Copyright. Please refer to the COPYRIGHT file
 * distributed with this source distribution.
 *
 * This file is part of OpenCPI <http://www.opencpi.org>
 *
 * OpenCPI is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <sstream>
#include <unistd.h>
#include <iomanip>
#include <math.h>
#include "OcpiApi.hh"

namespace OA = OCPI::API;
using namespace std;

#define PROMPT(app,val,worker,propmin,propmax) prompt_auto_range(app, val, #val, "Error: invalid "#val".\n",worker,propmin,propmax);

template <typename T>
std::string to_string(T const& value) {
  stringstream sstr;
  sstr << value;
  return sstr.str();
}

static void usage(const char *name, const char *error_message) {
  fprintf(stderr,
  "%s\n"
  "Usage is: %s <mode> <optional debug_mode>\n"
  "    mode       # Test mode of the application. Valid modes are 'rx', 'tx', 'txrx', 'filerw', or 'bbloopback'.\n"
  "    debug_mode # Optional debug mode which performs both an initial and final dump of all properties. Use 'd'.\n"
  "Example: FSK_10 filerw d\n",
  error_message,name);
  exit(1);
}

static void printLimits(const char *error_message, double current_value, double lower_limit, double upper_limit) {
  fprintf(stderr,
  "%s"
  "Value given of %f is outside the range of %f to %f.\n",
  error_message, current_value, lower_limit, upper_limit);
  exit(1);
}

/*! @brief Compare value x against min and max values read from OpenCPI app
 *         worker(worker)-specified min and max properties and exit (this)
 *         application when limits are invalid.
 ******************************************************************************/
template<typename T> void validate_limits(const T val,
                                          const char* error_message,
                                          T min, T max) {

  if((val<min) or (val>max))
  {
    printLimits(error_message, val, min, max);
  }
}

// just allows skipping creating the property object
template<typename T> T get_worker_prop_val(OA::Application& app,
    const char* worker_app_inst_name, const char* prop_name)
{
  OA::Property prop(app, worker_app_inst_name, prop_name);
  return prop.getValue<T>();
}

template<typename T> void prompt_with_range(T& val, const char* val_c_str,
    const char* error_message, T& min, T& max)
{
  cout << setprecision(15) << "Enter " << val_c_str << " [range("
       << min << " - " << max << ") default = "
       << val << "]" << endl;
  std::string input;
  std::getline(std::cin, input);
  if (!input.empty())
  {
    std::istringstream stream(input);
    stream >> val;
  }
  validate_limits(val, error_message, min, max);
}

template<typename T> void prompt_auto_range(OA::Application& app,T& val, const char* val_c_str,
    const char* error_message, const char* worker, const char* propmin, const char* propmax)
{
  T min = get_worker_prop_val<T>(app, worker, propmin);
  T max = get_worker_prop_val<T>(app, worker, propmax);

  prompt_with_range(val, val_c_str, error_message, min, max);
}

uint16_t get_zero_pad_num_zeros(OCPI::API::Application& app) {
  OA::Property p(app, "zero_pad", "num_zeros");
  uint16_t zero_pad_num_zeros = p.getUShortValue();
  return zero_pad_num_zeros;
}

/*! @return Currently configured (based on worker property values)
 *          CIC interpolation factor.
 ******************************************************************************/
uint16_t get_cic_interpolation_factor(OCPI::API::Application& app) {
  OA::Property p(app, "cic_int", "R");
  uint16_t cic_interpolation_factor = p.getUShortValue();
  return cic_interpolation_factor;
}

/*! @return Currently configured (based on worker property values)
 *          TX interpolation factor (combination of cic_int and zero_pad
 *          workers).
 ******************************************************************************/
double get_tx_interpolation_factor(OCPI::API::Application& app) {
  // x0, x1, ... -> zero_padding --> x0, [0, 0, ...], x1, [0 0 ..]
  // there are num zeros+1 samples out for every sample in
  uint16_t zero_pad_num_zeros = get_zero_pad_num_zeros(app);
  double zp_factor = ((double)zero_pad_num_zeros) + 1.;

  uint16_t cic_interpolation_factor = get_cic_interpolation_factor(app);

  double tx_interpolation_factor = zp_factor * ((double)cic_interpolation_factor);
  return tx_interpolation_factor;
}

/*! @return Currently configured (based on worker property values)
 *          TX sample rate in MHz.
 ******************************************************************************/
double get_tx_sample_rate_MHz(OCPI::API::Application& app) {
  OA::Property p(app, "tx", "sample_rate_MHz");
  double tx_sample_rate_MHz = p.getDoubleValue();
  return tx_sample_rate_MHz;
}

/*! @return Currently configured (based on worker property values)
 *          TX bit rate in bits/sec.
 ******************************************************************************/
double get_tx_bit_rate_bps(OCPI::API::Application& app) {
  double tx_sample_rate_MHz = get_tx_sample_rate_MHz(app);
  double tx_sample_rate_Hz = tx_sample_rate_MHz *1000000.;

  double rate = tx_sample_rate_Hz/get_tx_interpolation_factor(app);
  return rate;
}

// move file to local location from ram for rx and txrx modes
void move_file_if_rx_or_txrx(OA::Application& app,
    const std::string& mode) {

  if((mode == "rx") or (mode == "txrx")) {
    std::string fileName;
    app.getProperty("file_write","fileName",fileName);

    std::ostringstream oss;
    oss << "mv " << fileName << " odata/";
    oss << ((mode == "rx") ? "out_app_fsk_rx.bin" : "out_app_fsk_txrx.bin");
    const char* cmd = oss.str().c_str();

    int ret = system(cmd);
    if(ret != 0) {
      std::cerr <<"ERROR: "<< cmd <<" returned non-zero value of "<< ret <<"\n";
      exit(1);
    }
  }
}

int main(int argc, char **argv) {

  //Assign default values
  const char *argv0 = strrchr(argv[0], '/');
  argv0 = argv[0];
  bool overrunFlag = false;
  std::string mode, xml_name, input, value;
  bool debug_mode = false;
  double rx_sample_rate = 39.936;
  double rx_rf_center_freq = 999;
  double rx_rf_bw;
  double rx_rf_gain;
  double rx_bb_bw = 5;
  double rx_bb_gain = 42;
  double rx_if_center_freq = 1;
  double tx_sample_rate = 39.936;
  double tx_rf_center_freq = 1000;
  double tx_rf_gain = 4;
  double tx_bb_bw = 5;
  double tx_bb_gain = -4;
  //double tx_rf_bw = -1;

  //double tx_rf_bw_min;

  double rx_if_center_freq_max;
  double rx_if_center_freq_min;
  //double tx_rf_bw_max;


  double runtime = 15;
  enum HdlPlatform           {microzed_10_cc, picozed_10_cc};
  enum HdlPlatformRFFrontend {microzed_10_ccFrontend, picozed_10_ccFrontend};
  OA::Container *container;
  const HdlPlatformRFFrontend defaultFrontend = microzed_10_ccFrontend;
  HdlPlatformRFFrontend currentFrontend = defaultFrontend;

  //Check what platform we are on
  bool validContainerFound = false;
  for (unsigned n = 0; (container = OA::ContainerManager::get(n)); n++)
  {
    printf("container is %s\n", container->platform().c_str()) ;

    if (container->model() == "hdl" && container->platform() == "microzed_10_cc")
    {
      currentFrontend = microzed_10_ccFrontend;
      validContainerFound = true;
      printf("FSK_10 APP for MICROZED_10_CC\n");
    } 
    if (container->model() == "hdl" && container->platform() == "picozed_10_cc")
    {
      currentFrontend = picozed_10_ccFrontend;
      validContainerFound = true;
      printf("FSK_10 APP for PICOZED_10_CC\n");
    }
    if (validContainerFound)
    {
      break;
    }
  }

  if (!validContainerFound)
  { fprintf(stderr,"Error: not on a valid platform (no containers found)\n");
    return 1;
  }

  printf("model is %s, container is %s\n", container->model().c_str(), container->platform().c_str()) ;

  // for microzed_10_cc and picozed_10_cc frontends, wait until after app initialization and read min/max values from rx/tx workers

  try {
    //Check number of inputs and test mode
    if (argc > 1)
    {
      mode = argv[1];
      if ( (mode != "rx") && (mode != "tx") && (mode != "txrx") && (mode != "filerw") && (mode != "bbloopback") )
      {
        usage(argv0,"Error: incorrect test mode.\n");
      }
    }
    else
    {
      usage(argv0,"Error: wrong number of arguments.\n");
    }
    //Check for debug flag
    if (argc > 2)
    {
      std::string arg_debug_mode = argv[2];
      if (arg_debug_mode == "d")
      {
        debug_mode = true;
      }
    }

    //Assign the appropriate application XML file
    if(currentFrontend == microzed_10_ccFrontend)
    {
      if (mode == "rx")
      {
	xml_name = "app_fsk_rx_microzed_10_cc.xml";
      }
      else if (mode == "tx") {
	xml_name = "app_fsk_tx_microzed_10_cc.xml";
      }
      else if (mode == "txrx") {
	xml_name = "app_fsk_txrx_microzed_10_cc.xml";
      }
      else if (mode == "filerw") {
        xml_name = "app_fsk_filerw.xml";
      } 
      else
      {
        usage(argv0, "Error: incorrect test mode.\n");
      }
    }
    if(currentFrontend == picozed_10_ccFrontend)
    {
      if (mode == "rx")
      {
	xml_name = "app_fsk_rx_picozed_10_cc.xml";
      }
      else if (mode == "tx") {
	xml_name = "app_fsk_tx_picozed_10_cc.xml";
      }
      else if (mode == "txrx") {
	xml_name = "app_fsk_txrx_picozed_10_cc.xml";
      }
      else if (mode == "filerw") {
	xml_name = "app_fsk_filerw.xml";
      }
      else
      {
        usage(argv0, "Error: incorrect test mode.\n");
      }
    }

    printf("Application properties are found in XML file: %s\n", xml_name.c_str());

    //Prompt the user for the amount of time to run for all modes except filerw
    if (mode == "rx" || mode == "tx" || mode == "txrx" || mode == "bbloopback")
    {
      switch (currentFrontend)
      {
        case microzed_10_ccFrontend:
			   break;
        case picozed_10_ccFrontend:
                           break;

        default: cout << "Invalid frontend choice.";
                 exit(1);
      }

      cout << setprecision(5) << "Enter run time in seconds [default = "
                       << runtime << "]" << endl;
      std::getline(std::cin, input);
      if (!input.empty())
      {
        std::istringstream stream(input);
        stream >> runtime;
      }
    }

    OA::Application app(xml_name.c_str(), NULL);
    app.initialize();
    printf("App initialized.\n");

    if(currentFrontend == microzed_10_ccFrontend || currentFrontend == picozed_10_ccFrontend)
    {
      if ((mode == "rx") || (mode == "txrx"))
      {	  
        app.setProperty("rx", "config", "duplex_mode FDD,SMA_channel RX");
      }
      
      if ((mode == "tx") || (mode == "txrx"))
      {
        app.setProperty("tx", "config", "duplex_mode FDD,SMA_channel TX");
      }

    }

    //Prompt the user for rx settings
    //Verify rx inputs are within valid ranges
    if ((mode == "rx" || mode == "txrx") and
	 (currentFrontend == microzed_10_ccFrontend || currentFrontend == picozed_10_ccFrontend) )
    {
      // set known-good defaults for microzed_10_cc or pizozed_10_cc
      rx_sample_rate = 4.;
      rx_rf_center_freq = 2400.;
      rx_rf_bw = -1.;
      rx_bb_bw = 4.;
      rx_bb_gain = -1.;
      rx_rf_gain = 24.;

      PROMPT(app, rx_sample_rate,    "rx", "sample_rate_min_MHz",         "sample_rate_max_MHz"         );
      app.setPropertyValue<double>(  "rx", "sample_rate_MHz",             rx_sample_rate);

      PROMPT(app, rx_rf_center_freq, "rx", "frequency_min_MHz",           "frequency_max_MHz"           );
      app.setPropertyValue<double>(  "rx", "frequency_MHz",               rx_rf_center_freq);

      PROMPT(app, rx_rf_bw,          "rx", "rf_cutoff_frequency_min_MHz", "rf_cutoff_frequency_max_MHz" );
      app.setPropertyValue<double>(  "rx", "rf_cutoff_frequency_MHz",     rx_rf_bw);

      PROMPT(app, rx_rf_gain,        "rx", "rf_gain_min_dB",              "rf_gain_max_dB"             );
      app.setPropertyValue<double>(  "rx", "rf_gain_dB",                  rx_rf_gain);

      PROMPT(app, rx_bb_bw,          "rx", "bb_cutoff_frequency_min_MHz", "bb_cutoff_frequency_max_MHz");
      app.setPropertyValue<double>(  "rx", "bb_cutoff_frequency_MHz",     rx_bb_bw);

      PROMPT(app, rx_bb_gain,        "rx", "bb_gain_min_dB",              "bb_gain_max_dB"             );
      app.setPropertyValue<double>(  "rx", "bb_gain_dB",                  rx_bb_gain);

      rx_if_center_freq_min = rx_sample_rate*-0.5;
      rx_if_center_freq_max = rx_sample_rate*32767/65536;
      rx_if_center_freq = 0; // set default for prompt

      prompt_with_range(rx_if_center_freq, "rx_if_center_freq", "Error: invalid rx_if_center_freq.\n", rx_if_center_freq_min, rx_if_center_freq_max);

      if (rx_if_center_freq == 0.)
      {
        app.setProperty("complex_mixer", "enable", "false");
      }
      else
      {
        // It is desired that setting a + IF freq results in mixing *down*.
        // Because complex_mixer's NCO mixes *up* for + freqs (see complex mixer
        // datasheet), IF tune freq must be negated in order to achieve the
        // desired effect.
        double nco_output_freq = -rx_if_center_freq;

        // todo this math might be better off in a small proxy that sits on top of complex_mixer
        // from complex mixer datasheet, nco_output_freq =
        // sample_freq * phs_inc / 2^phs_acc_width, phs_acc_width is fixed at 16
        OA::Short phase_inc = round(nco_output_freq/rx_sample_rate*65536.);

        //std::cout << "setting complex mixer phase_inc = " << phase_inc <<"\n";
        app.setPropertyValue<OA::Short>("complex_mixer","phs_inc", phase_inc);
      }
    }

    //Prompt the user for tx settings
    //Verify tx inputs are within valid ranges
    if ((mode == "tx" || mode == "txrx") and
	 (currentFrontend == microzed_10_ccFrontend || currentFrontend == picozed_10_ccFrontend))
    {
      // set known-good defaults for microzed_10_cc or picozed_10_cc
      tx_sample_rate = 4.;
      tx_rf_center_freq = 2400.;
      double tx_rf_bw = -1.;
      tx_bb_bw = 4.;
      tx_bb_gain = -1.;
      tx_rf_gain = -15.;

      PROMPT(app, tx_sample_rate,    "tx", "sample_rate_min_MHz",         "sample_rate_max_MHz"        );
      app.setPropertyValue<double>(  "tx", "sample_rate_MHz",             tx_sample_rate);

      PROMPT(app, tx_rf_center_freq, "tx", "frequency_min_MHz",           "frequency_max_MHz"          );
      app.setPropertyValue<double>(  "tx", "frequency_MHz",               tx_rf_center_freq);

      PROMPT(app, tx_rf_bw,          "tx", "rf_cutoff_frequency_min_MHz", "rf_cutoff_frequency_max_MHz");
      app.setPropertyValue<double>(  "tx", "rf_cutoff_frequency_MHz",     tx_rf_bw);

      PROMPT(app, tx_rf_gain,         "tx", "rf_gain_min_dB",              "rf_gain_max_dB"             );
      app.setPropertyValue<double>(  "tx", "rf_gain_dB",                  tx_rf_gain);

      PROMPT(app, tx_bb_bw,          "tx", "bb_cutoff_frequency_min_MHz", "bb_cutoff_frequency_max_MHz");
      app.setPropertyValue<double>(  "tx", "bb_cutoff_frequency_MHz",     tx_bb_bw);

      PROMPT(app, tx_bb_gain,        "tx", "bb_gain_min_dB",              "bb_gain_max_dB"             );
      app.setPropertyValue<double>(  "tx", "bb_gain_dB",                  tx_bb_gain);
    }

    //Verify runtime is within a valid range
    if (mode == "rx" || mode == "tx" || mode == "txrx" || mode == "bbloopback")
    {
      if (runtime < 1 || runtime > 60)
      {
        printLimits("Error: invalid runtime.\n", runtime, 1, 60);
      }
    }

    //Loopback mode for txrx app
    if (mode == "bbloopback")
    {
      //Disable complex mixer, dc offset filter
      app.setProperty("complex_mixer", "enable", "false");
      app.setProperty("dc_offset_filter","bypass", "true");
      //Override filewrite name
      app.setProperty("file_write","fileName", "odata/out_app_fsk_bbloopback.bin");
    }

    // dump all inital properties following initalization but before start
    if (debug_mode) {
      std::string name, value;
      bool isParameter, hex = false;
      fprintf(stderr, "Dump of all initial property values:\n");
      for (unsigned n = 0; app.getProperty(n, name, value, hex, &isParameter); n++)
      {
        fprintf(stderr, "Property %2u: %s = \"%s\"%s\n", n, name.c_str(), value.c_str(), isParameter ? " (parameter)" : "");
      }
    }

    app.start();
    printf("App started.\n");


    //Mode filerw waits for a zlm message to complete. All others use runtime.
    if (mode == "rx" || mode == "tx" || mode == "txrx" || mode == "bbloopback")
    {
      printf("App runs for %f seconds...\n",runtime);
      while (runtime > 0)
      {
        sleep(1);
        runtime --;
        if (mode == "rx" || mode == "txrx" || mode == "bbloopback")
        {
          app.getProperty("qadc","overrun", value);
          if (strcmp("true", value.c_str()) == 0)
          {
            overrunFlag  = true;
          }
        }
      }
    }
    else
    {
      printf("Waiting for done signal from file_write.\n");
      app.wait();
    }

    app.stop();
    printf("App stopped.\n");


    // dump all final property values
    if (debug_mode) {
      std::string name, value;
      bool isParameter, hex = false;
      fprintf(stderr, "Dump of all final property values:\n");
      for (unsigned n = 0; app.getProperty(n, name, value, hex, &isParameter); n++)
      {
        if (!isParameter)
        {
          fprintf(stderr, "Property %2u: %s = \"%s\"\n", n, name.c_str(), value.c_str());
        }
        else
        {
          fprintf(stderr, "Parameter property %2u: %s = \"%s\"\n", n, name.c_str(), value.c_str());
        }
      }
    }

    //Mode tx is the only one that doesn't write to a file
    if (mode == "rx" || mode == "txrx" || mode == "filerw" || mode == "bbloopback")
    {
      app.getProperty("file_write","bytesWritten", value);
      printf("Bytes to file : %s\n", value.c_str());
      if (overrunFlag)
      {
        printf("WARNING: RX sample rate was high enough that data could not be written to file fast enough, resulting in samples being dropped, try a lower RX sample rate\n");
      }
    }

    //Query tx peak values
    if (mode == "tx" || mode == "txrx" || mode == "bbloopback")
    {
      printf("TX Bit Rate            = %f bps\n",get_tx_bit_rate_bps(app));
    }
    if (mode == "tx" || mode == "txrx" || mode == "filerw" || mode == "bbloopback")
    {
      app.getProperty("tx_fir_real","peak", value);
      printf("TX FIR Real Peak       = %s\n", value.c_str());
      app.getProperty("phase_to_amp_cordic","magnitude", value);
      printf("Phase to Amp Magnitude = %s\n", value.c_str());

    }

    //Query rx peak values
    if (mode == "rx" || mode == "txrx" || mode == "filerw" || mode == "bbloopback")
    {
      if (mode != "filerw")
      {
        app.getProperty("dc_offset_filter","peak", value);
        printf("DC Offset Peak         = %s\n", value.c_str());
        app.getProperty("iq_imbalance_fixer","peak", value);
        printf("IQ Imbalance Peak      = %s\n", value.c_str());
        app.getProperty("complex_mixer","peak", value);
        printf("Complex Mixer Peak     = %s\n", value.c_str());
        app.getProperty("qadc","overrun", value);
        printf("ADC Samples Dropped    = %s\n", value.c_str());
      }
      app.getProperty("rp_cordic","magnitude", value);
      printf("RP Cordic Magnitude    = %s\n", value.c_str());
      app.getProperty("rx_fir_real","peak", value);
      printf("RX FIR Real Peak       = %s\n", value.c_str());
    }

    move_file_if_rx_or_txrx(app, mode); // exits if failure

    printf("Application complete\n");

  } catch (std::string &e) {
    fprintf(stderr, "Exception thrown: %s\n", e.c_str());
    return 1;
  }

  return 0;
}
