# [v2.4.7](https://gitlab.com/opencpi/osp/ocpi.osp.avnet/-/compare/v2.4.6...v2.4.7) (2024-01-10)

Changes/additions since [OpenCPI Release v2.4.6](https://gitlab.com/opencpi/opencpi/-/releases/v2.4.6)

### Bug Fixes
- **devops**: prevent commits from automatically launching a CI/CD pipeline. (!16)(091798f0)

### Enhancements
- **osp**: add support for microzed and picozed development boards. (!5)(9fb94a35)

### Miscellaneous
- **devops**: add .gitlab-ci.yml. (!7)(bcbf0ea0)
- **osp**: add .gitignore and issue templates. (!6)(9a3d1d55)
- **osp**: fix `spec=` attributes in XML files. (!15)(b6c93bfb)

# [v2.4.6](https://gitlab.com/opencpi/osp/ocpi.osp.avnet/-/compare/v2.4.5...v2.4.6) (2023-03-29)

No Changes/additions since [OpenCPI Release v2.4.5](https://gitlab.com/opencpi/opencpi/-/releases/v2.4.5)

# [v2.4.5](https://gitlab.com/opencpi/osp/ocpi.osp.avnet/-/compare/v2.4.4...v2.4.5) (2023-03-16)

No Changes/additions since [OpenCPI Release v2.4.4](https://gitlab.com/opencpi/opencpi/-/releases/v2.4.4)

# [v2.4.4](https://gitlab.com/opencpi/osp/ocpi.osp.avnet/-/compare/v2.4.3...v2.4.4) (2023-01-22)

No Changes/additions since [OpenCPI Release v2.4.3](https://gitlab.com/opencpi/opencpi/-/releases/v2.4.3)

# [v2.4.3](https://gitlab.com/opencpi/osp/ocpi.osp.avnet/-/compare/v2.4.2...v2.4.3) (2022-10-11)

Changes/additions since [OpenCPI Release v2.4.2](https://gitlab.com/opencpi/opencpi/-/releases/v2.4.2)

### Enhancements
- **doc**: add RST files that document OpenCPI setup for MicroZed systems. (!8)(44f0d781)
- **doc**: add RST files that document OpenCPI setup for PicoZed systems. (!8)(44f0d781)
- **doc**: update OSP README.md to reflect latest Avnet OSP docs and their locations. (!13)(2fa8f0ca)

### Bug Fixes
- **doc**: fix LaTeX documentation. (!9)(b7d5d257)
- **doc**: rename avnet-project.rst to index.rst. (!11)(925b5041)

### Miscellaneous
- **doc**: remove directory that contains obsolete/redundant getting started guide files. (!12)(79313854)
- **doc,osp**: delete redundant `snippets` directories. (!10)(b3b67640)

# [v2.4.2](https://gitlab.com/opencpi/osp/ocpi.osp.avnet/-/compare/52591ce4...v2.4.2) (2022-05-26)

Initial release of MicroZed & PicoZed OSPs
