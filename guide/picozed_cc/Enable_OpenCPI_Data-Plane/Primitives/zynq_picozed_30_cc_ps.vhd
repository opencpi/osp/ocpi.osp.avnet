
library IEEE; use IEEE.std_logic_1164.all, ieee.numeric_std.all;
library zynq_picozed_30_cc; use zynq_picozed_30_cc.zynq_picozed_30_cc_pkg.all;
library axi;

entity zynq_picozed_30_cc_ps is
  generic (package_name : string  := "sbg485";
           dq_width     : natural := 32);
  port (ps_in            : in  pl2ps_t;
           ps_out        : out ps2pl_t;
           m_axi_gp_in   : in  axi.zynq_7000_m_gp.axi_s2m_array_t(0 to C_M_AXI_GP_COUNT-1);
           m_axi_gp_out  : out axi.zynq_7000_m_gp.axi_m2s_array_t(0 to C_M_AXI_GP_COUNT-1);
           s_axi_hp_in   : in  axi.zynq_7000_s_hp.axi_m2s_array_t(0 to C_S_AXI_HP_COUNT-1);
           s_axi_hp_out  : out axi.zynq_7000_s_hp.axi_s2m_array_t(0 to C_S_AXI_HP_COUNT-1);
           s_axi_acp_in  : in  axi.zynq_7000_s_hp.axi_m2s_t;
           s_axi_acp_out : out axi.zynq_7000_s_hp.axi_s2m_t);
end entity zynq_picozed_30_cc_ps;

architecture rtl of zynq_picozed_30_cc_ps is

  constant C_AXI_ADDR_WIDTH     : natural := 32;
  constant C_S_AXI_ACP_ID_WIDTH : natural := 3;

  -----------------------------------
  --ADD THESE CONSTANT DECLARATIONS
  constant C_M_AXI_GP0_THREAD_ID_WIDTH : natural := 12;
  constant C_MIO_PRIMITIVE             : natural := 54;
  constant C_DM_WIDTH                  : natural := 4;
  constant C_DQ_WIDTH                  : natural := 32;
  constant C_DQS_WIDTH                 : natural := 4;
  -----------------------------------

  -- We need this unfortunate redundant component declaration for two reasons:
  -- 1. Vivado claims that you must have a component declaration to instance verilog from vhdl
  -- 2. This relieves any ordering dependency between the verilog module and this file.
  component preset_config_wrapper_processing_system7_0_0 is  -- For Reference Design use: PZ7030_FMC2_ps7_0
    port (
      --SDIO0_WP      : in std_logic;   -- Use for Reference Design
      TTC0_WAVE0_OUT      : out std_logic;
      TTC0_WAVE1_OUT      : out std_logic;
      TTC0_WAVE2_OUT      : out std_logic;
      USB0_PORT_INDCTL    : out std_logic_vector(1 downto 0);
      USB0_VBUS_PWRSELECT : out std_logic;
      USB0_VBUS_PWRFAULT  : in  std_logic;

      M_AXI_GP0_ARVALID : out std_logic;
      M_AXI_GP0_AWVALID : out std_logic;
      M_AXI_GP0_BREADY  : out std_logic;
      M_AXI_GP0_RREADY  : out std_logic;
      M_AXI_GP0_WLAST   : out std_logic;
      M_AXI_GP0_WVALID  : out std_logic;
      M_AXI_GP0_ARID    : out std_logic_vector(C_M_AXI_GP0_THREAD_ID_WIDTH-1 downto 0);
      M_AXI_GP0_AWID    : out std_logic_vector(C_M_AXI_GP0_THREAD_ID_WIDTH-1 downto 0);
      M_AXI_GP0_WID     : out std_logic_vector(C_M_AXI_GP0_THREAD_ID_WIDTH-1 downto 0);
      M_AXI_GP0_ARBURST : out std_logic_vector(1 downto 0);
      M_AXI_GP0_ARLOCK  : out std_logic_vector(1 downto 0);
      M_AXI_GP0_ARSIZE  : out std_logic_vector(2 downto 0);
      M_AXI_GP0_AWBURST : out std_logic_vector(1 downto 0);
      M_AXI_GP0_AWLOCK  : out std_logic_vector(1 downto 0);
      M_AXI_GP0_AWSIZE  : out std_logic_vector(2 downto 0);
      M_AXI_GP0_ARPROT  : out std_logic_vector(2 downto 0);
      M_AXI_GP0_AWPROT  : out std_logic_vector(2 downto 0);
      M_AXI_GP0_ARADDR  : out std_logic_vector(C_AXI_ADDR_WIDTH-1 downto 0);
      M_AXI_GP0_AWADDR  : out std_logic_vector(C_AXI_ADDR_WIDTH-1 downto 0);
      M_AXI_GP0_WDATA   : out std_logic_vector(31 downto 0);
      M_AXI_GP0_ARCACHE : out std_logic_vector(3 downto 0);
      M_AXI_GP0_ARLEN   : out std_logic_vector(3 downto 0);
      M_AXI_GP0_ARQOS   : out std_logic_vector(3 downto 0);
      M_AXI_GP0_AWCACHE : out std_logic_vector(3 downto 0);
      M_AXI_GP0_AWLEN   : out std_logic_vector(3 downto 0);
      M_AXI_GP0_AWQOS   : out std_logic_vector(3 downto 0);
      M_AXI_GP0_WSTRB   : out std_logic_vector(3 downto 0);
      M_AXI_GP0_ACLK    : in  std_logic;
      M_AXI_GP0_ARREADY : in  std_logic;
      M_AXI_GP0_AWREADY : in  std_logic;
      M_AXI_GP0_BVALID  : in  std_logic;
      M_AXI_GP0_RLAST   : in  std_logic;
      M_AXI_GP0_RVALID  : in  std_logic;
      M_AXI_GP0_WREADY  : in  std_logic;
      M_AXI_GP0_BID     : in  std_logic_vector(C_M_AXI_GP0_THREAD_ID_WIDTH-1 downto 0);
      M_AXI_GP0_RID     : in  std_logic_vector(C_M_AXI_GP0_THREAD_ID_WIDTH-1 downto 0);
      M_AXI_GP0_BRESP   : in  std_logic_vector(1 downto 0);
      M_AXI_GP0_RRESP   : in  std_logic_vector(1 downto 0);
      M_AXI_GP0_RDATA   : in  std_logic_vector(31 downto 0);

      --S_AXI_ACP_ARESETN : out std_logic;
      S_AXI_ACP_AWREADY : out std_logic;
      S_AXI_ACP_ARREADY : out std_logic;
      S_AXI_ACP_BVALID  : out std_logic;
      S_AXI_ACP_RLAST   : out std_logic;
      S_AXI_ACP_RVALID  : out std_logic;
      S_AXI_ACP_WREADY  : out std_logic;
      S_AXI_ACP_BRESP   : out std_logic_vector(1 downto 0);
      S_AXI_ACP_RRESP   : out std_logic_vector(1 downto 0);
      S_AXI_ACP_BID     : out std_logic_vector(C_S_AXI_ACP_ID_WIDTH-1 downto 0);
      S_AXI_ACP_RID     : out std_logic_vector(C_S_AXI_ACP_ID_WIDTH-1 downto 0);
      S_AXI_ACP_RDATA   : out std_logic_vector(63 downto 0);
      S_AXI_ACP_ACLK    : in  std_logic;
      S_AXI_ACP_ARVALID : in  std_logic;
      S_AXI_ACP_AWVALID : in  std_logic;
      S_AXI_ACP_BREADY  : in  std_logic;
      S_AXI_ACP_RREADY  : in  std_logic;
      S_AXI_ACP_WLAST   : in  std_logic;
      S_AXI_ACP_WVALID  : in  std_logic;
      S_AXI_ACP_ARID    : in  std_logic_vector(C_S_AXI_ACP_ID_WIDTH-1 downto 0);
      S_AXI_ACP_ARPROT  : in  std_logic_vector(2 downto 0);
      S_AXI_ACP_AWID    : in  std_logic_vector(C_S_AXI_ACP_ID_WIDTH-1 downto 0);
      S_AXI_ACP_AWPROT  : in  std_logic_vector(2 downto 0);
      S_AXI_ACP_WID     : in  std_logic_vector(C_S_AXI_ACP_ID_WIDTH-1 downto 0);
      S_AXI_ACP_ARADDR  : in  std_logic_vector(C_AXI_ADDR_WIDTH-1 downto 0);
      S_AXI_ACP_AWADDR  : in  std_logic_vector(C_AXI_ADDR_WIDTH-1 downto 0);
      S_AXI_ACP_ARCACHE : in  std_logic_vector(3 downto 0);
      S_AXI_ACP_ARLEN   : in  std_logic_vector(3 downto 0);
      S_AXI_ACP_ARQOS   : in  std_logic_vector(3 downto 0);
      S_AXI_ACP_AWCACHE : in  std_logic_vector(3 downto 0);
      S_AXI_ACP_AWLEN   : in  std_logic_vector(3 downto 0);
      S_AXI_ACP_AWQOS   : in  std_logic_vector(3 downto 0);
      S_AXI_ACP_ARBURST : in  std_logic_vector(1 downto 0);
      S_AXI_ACP_ARLOCK  : in  std_logic_vector(1 downto 0);
      S_AXI_ACP_ARSIZE  : in  std_logic_vector(2 downto 0);
      S_AXI_ACP_AWBURST : in  std_logic_vector(1 downto 0);
      S_AXI_ACP_AWLOCK  : in  std_logic_vector(1 downto 0);
      S_AXI_ACP_AWSIZE  : in  std_logic_vector(2 downto 0);
      S_AXI_ACP_ARUSER  : in  std_logic_vector(4 downto 0);
      S_AXI_ACP_AWUSER  : in  std_logic_vector(4 downto 0);
      S_AXI_ACP_WDATA   : in  std_logic_vector(63 downto 0);
      S_AXI_ACP_WSTRB   : in  std_logic_vector(7 downto 0);

      S_AXI_HP0_ARREADY        : out std_logic;
      S_AXI_HP0_AWREADY        : out std_logic;
      S_AXI_HP0_BVALID         : out std_logic;
      S_AXI_HP0_RLAST          : out std_logic;
      S_AXI_HP0_RVALID         : out std_logic;
      S_AXI_HP0_WREADY         : out std_logic;
      S_AXI_HP0_BRESP          : out std_logic_vector (1 downto 0);
      S_AXI_HP0_RRESP          : out std_logic_vector (1 downto 0);
      S_AXI_HP0_BID            : out std_logic_vector (5 downto 0);
      S_AXI_HP0_RID            : out std_logic_vector (5 downto 0);
      S_AXI_HP0_RDATA          : out std_logic_vector (63 downto 0);
      S_AXI_HP0_RCOUNT         : out std_logic_vector (7 downto 0);
      S_AXI_HP0_WCOUNT         : out std_logic_vector (7 downto 0);
      S_AXI_HP0_RACOUNT        : out std_logic_vector (2 downto 0);
      S_AXI_HP0_WACOUNT        : out std_logic_vector (5 downto 0);
      S_AXI_HP0_ACLK           : in  std_logic;
      S_AXI_HP0_ARVALID        : in  std_logic;
      S_AXI_HP0_AWVALID        : in  std_logic;
      S_AXI_HP0_BREADY         : in  std_logic;
      S_AXI_HP0_RDISSUECAP1_EN : in  std_logic;
      S_AXI_HP0_RREADY         : in  std_logic;
      S_AXI_HP0_WLAST          : in  std_logic;
      S_AXI_HP0_WRISSUECAP1_EN : in  std_logic;
      S_AXI_HP0_WVALID         : in  std_logic;
      S_AXI_HP0_ARBURST        : in  std_logic_vector (1 downto 0);
      S_AXI_HP0_ARLOCK         : in  std_logic_vector (1 downto 0);
      S_AXI_HP0_ARSIZE         : in  std_logic_vector (2 downto 0);
      S_AXI_HP0_AWBURST        : in  std_logic_vector (1 downto 0);
      S_AXI_HP0_AWLOCK         : in  std_logic_vector (1 downto 0);
      S_AXI_HP0_AWSIZE         : in  std_logic_vector (2 downto 0);
      S_AXI_HP0_ARPROT         : in  std_logic_vector (2 downto 0);
      S_AXI_HP0_AWPROT         : in  std_logic_vector (2 downto 0);
      S_AXI_HP0_ARADDR         : in  std_logic_vector (31 downto 0);
      S_AXI_HP0_AWADDR         : in  std_logic_vector (31 downto 0);
      S_AXI_HP0_ARCACHE        : in  std_logic_vector (3 downto 0);
      S_AXI_HP0_ARLEN          : in  std_logic_vector (3 downto 0);
      S_AXI_HP0_ARQOS          : in  std_logic_vector (3 downto 0);
      S_AXI_HP0_AWCACHE        : in  std_logic_vector (3 downto 0);
      S_AXI_HP0_AWLEN          : in  std_logic_vector (3 downto 0);
      S_AXI_HP0_AWQOS          : in  std_logic_vector (3 downto 0);
      S_AXI_HP0_ARID           : in  std_logic_vector (5 downto 0);
      S_AXI_HP0_AWID           : in  std_logic_vector (5 downto 0);
      S_AXI_HP0_WID            : in  std_logic_vector (5 downto 0);
      S_AXI_HP0_WDATA          : in  std_logic_vector (63 downto 0);
      S_AXI_HP0_WSTRB          : in  std_logic_vector (7 downto 0);

      S_AXI_HP1_ARREADY        : out std_logic;
      S_AXI_HP1_AWREADY        : out std_logic;
      S_AXI_HP1_BVALID         : out std_logic;
      S_AXI_HP1_RLAST          : out std_logic;
      S_AXI_HP1_RVALID         : out std_logic;
      S_AXI_HP1_WREADY         : out std_logic;
      S_AXI_HP1_BRESP          : out std_logic_vector (1 downto 0);
      S_AXI_HP1_RRESP          : out std_logic_vector (1 downto 0);
      S_AXI_HP1_BID            : out std_logic_vector (5 downto 0);
      S_AXI_HP1_RID            : out std_logic_vector (5 downto 0);
      S_AXI_HP1_RDATA          : out std_logic_vector (63 downto 0);
      S_AXI_HP1_RCOUNT         : out std_logic_vector (7 downto 0);
      S_AXI_HP1_WCOUNT         : out std_logic_vector (7 downto 0);
      S_AXI_HP1_RACOUNT        : out std_logic_vector (2 downto 0);
      S_AXI_HP1_WACOUNT        : out std_logic_vector (5 downto 0);
      S_AXI_HP1_ACLK           : in  std_logic;
      S_AXI_HP1_ARVALID        : in  std_logic;
      S_AXI_HP1_AWVALID        : in  std_logic;
      S_AXI_HP1_BREADY         : in  std_logic;
      S_AXI_HP1_RDISSUECAP1_EN : in  std_logic;
      S_AXI_HP1_RREADY         : in  std_logic;
      S_AXI_HP1_WLAST          : in  std_logic;
      S_AXI_HP1_WRISSUECAP1_EN : in  std_logic;
      S_AXI_HP1_WVALID         : in  std_logic;
      S_AXI_HP1_ARBURST        : in  std_logic_vector (1 downto 0);
      S_AXI_HP1_ARLOCK         : in  std_logic_vector (1 downto 0);
      S_AXI_HP1_ARSIZE         : in  std_logic_vector (2 downto 0);
      S_AXI_HP1_AWBURST        : in  std_logic_vector (1 downto 0);
      S_AXI_HP1_AWLOCK         : in  std_logic_vector (1 downto 0);
      S_AXI_HP1_AWSIZE         : in  std_logic_vector (2 downto 0);
      S_AXI_HP1_ARPROT         : in  std_logic_vector (2 downto 0);
      S_AXI_HP1_AWPROT         : in  std_logic_vector (2 downto 0);
      S_AXI_HP1_ARADDR         : in  std_logic_vector (31 downto 0);
      S_AXI_HP1_AWADDR         : in  std_logic_vector (31 downto 0);
      S_AXI_HP1_ARCACHE        : in  std_logic_vector (3 downto 0);
      S_AXI_HP1_ARLEN          : in  std_logic_vector (3 downto 0);
      S_AXI_HP1_ARQOS          : in  std_logic_vector (3 downto 0);
      S_AXI_HP1_AWCACHE        : in  std_logic_vector (3 downto 0);
      S_AXI_HP1_AWLEN          : in  std_logic_vector (3 downto 0);
      S_AXI_HP1_AWQOS          : in  std_logic_vector (3 downto 0);
      S_AXI_HP1_ARID           : in  std_logic_vector (5 downto 0);
      S_AXI_HP1_AWID           : in  std_logic_vector (5 downto 0);
      S_AXI_HP1_WID            : in  std_logic_vector (5 downto 0);
      S_AXI_HP1_WDATA          : in  std_logic_vector (63 downto 0);
      S_AXI_HP1_WSTRB          : in  std_logic_vector (7 downto 0);

      S_AXI_HP2_ARREADY        : out std_logic;
      S_AXI_HP2_AWREADY        : out std_logic;
      S_AXI_HP2_BVALID         : out std_logic;
      S_AXI_HP2_RLAST          : out std_logic;
      S_AXI_HP2_RVALID         : out std_logic;
      S_AXI_HP2_WREADY         : out std_logic;
      S_AXI_HP2_BRESP          : out std_logic_vector (1 downto 0);
      S_AXI_HP2_RRESP          : out std_logic_vector (1 downto 0);
      S_AXI_HP2_BID            : out std_logic_vector (5 downto 0);
      S_AXI_HP2_RID            : out std_logic_vector (5 downto 0);
      S_AXI_HP2_RDATA          : out std_logic_vector (63 downto 0);
      S_AXI_HP2_RCOUNT         : out std_logic_vector (7 downto 0);
      S_AXI_HP2_WCOUNT         : out std_logic_vector (7 downto 0);
      S_AXI_HP2_RACOUNT        : out std_logic_vector (2 downto 0);
      S_AXI_HP2_WACOUNT        : out std_logic_vector (5 downto 0);
      S_AXI_HP2_ACLK           : in  std_logic;
      S_AXI_HP2_ARVALID        : in  std_logic;
      S_AXI_HP2_AWVALID        : in  std_logic;
      S_AXI_HP2_BREADY         : in  std_logic;
      S_AXI_HP2_RDISSUECAP1_EN : in  std_logic;
      S_AXI_HP2_RREADY         : in  std_logic;
      S_AXI_HP2_WLAST          : in  std_logic;
      S_AXI_HP2_WRISSUECAP1_EN : in  std_logic;
      S_AXI_HP2_WVALID         : in  std_logic;
      S_AXI_HP2_ARBURST        : in  std_logic_vector (1 downto 0);
      S_AXI_HP2_ARLOCK         : in  std_logic_vector (1 downto 0);
      S_AXI_HP2_ARSIZE         : in  std_logic_vector (2 downto 0);
      S_AXI_HP2_AWBURST        : in  std_logic_vector (1 downto 0);
      S_AXI_HP2_AWLOCK         : in  std_logic_vector (1 downto 0);
      S_AXI_HP2_AWSIZE         : in  std_logic_vector (2 downto 0);
      S_AXI_HP2_ARPROT         : in  std_logic_vector (2 downto 0);
      S_AXI_HP2_AWPROT         : in  std_logic_vector (2 downto 0);
      S_AXI_HP2_ARADDR         : in  std_logic_vector (31 downto 0);
      S_AXI_HP2_AWADDR         : in  std_logic_vector (31 downto 0);
      S_AXI_HP2_ARCACHE        : in  std_logic_vector (3 downto 0);
      S_AXI_HP2_ARLEN          : in  std_logic_vector (3 downto 0);
      S_AXI_HP2_ARQOS          : in  std_logic_vector (3 downto 0);
      S_AXI_HP2_AWCACHE        : in  std_logic_vector (3 downto 0);
      S_AXI_HP2_AWLEN          : in  std_logic_vector (3 downto 0);
      S_AXI_HP2_AWQOS          : in  std_logic_vector (3 downto 0);
      S_AXI_HP2_ARID           : in  std_logic_vector (5 downto 0);
      S_AXI_HP2_AWID           : in  std_logic_vector (5 downto 0);
      S_AXI_HP2_WID            : in  std_logic_vector (5 downto 0);
      S_AXI_HP2_WDATA          : in  std_logic_vector (63 downto 0);
      S_AXI_HP2_WSTRB          : in  std_logic_vector (7 downto 0);

      S_AXI_HP3_ARREADY        : out std_logic;
      S_AXI_HP3_AWREADY        : out std_logic;
      S_AXI_HP3_BVALID         : out std_logic;
      S_AXI_HP3_RLAST          : out std_logic;
      S_AXI_HP3_RVALID         : out std_logic;
      S_AXI_HP3_WREADY         : out std_logic;
      S_AXI_HP3_BRESP          : out std_logic_vector (1 downto 0);
      S_AXI_HP3_RRESP          : out std_logic_vector (1 downto 0);
      S_AXI_HP3_BID            : out std_logic_vector (5 downto 0);
      S_AXI_HP3_RID            : out std_logic_vector (5 downto 0);
      S_AXI_HP3_RDATA          : out std_logic_vector (63 downto 0);
      S_AXI_HP3_RCOUNT         : out std_logic_vector (7 downto 0);
      S_AXI_HP3_WCOUNT         : out std_logic_vector (7 downto 0);
      S_AXI_HP3_RACOUNT        : out std_logic_vector (2 downto 0);
      S_AXI_HP3_WACOUNT        : out std_logic_vector (5 downto 0);
      S_AXI_HP3_ACLK           : in  std_logic;
      S_AXI_HP3_ARVALID        : in  std_logic;
      S_AXI_HP3_AWVALID        : in  std_logic;
      S_AXI_HP3_BREADY         : in  std_logic;
      S_AXI_HP3_RDISSUECAP1_EN : in  std_logic;
      S_AXI_HP3_RREADY         : in  std_logic;
      S_AXI_HP3_WLAST          : in  std_logic;
      S_AXI_HP3_WRISSUECAP1_EN : in  std_logic;
      S_AXI_HP3_WVALID         : in  std_logic;
      S_AXI_HP3_ARBURST        : in  std_logic_vector (1 downto 0);
      S_AXI_HP3_ARLOCK         : in  std_logic_vector (1 downto 0);
      S_AXI_HP3_ARSIZE         : in  std_logic_vector (2 downto 0);
      S_AXI_HP3_AWBURST        : in  std_logic_vector (1 downto 0);
      S_AXI_HP3_AWLOCK         : in  std_logic_vector (1 downto 0);
      S_AXI_HP3_AWSIZE         : in  std_logic_vector (2 downto 0);
      S_AXI_HP3_ARPROT         : in  std_logic_vector (2 downto 0);
      S_AXI_HP3_AWPROT         : in  std_logic_vector (2 downto 0);
      S_AXI_HP3_ARADDR         : in  std_logic_vector (31 downto 0);
      S_AXI_HP3_AWADDR         : in  std_logic_vector (31 downto 0);
      S_AXI_HP3_ARCACHE        : in  std_logic_vector (3 downto 0);
      S_AXI_HP3_ARLEN          : in  std_logic_vector (3 downto 0);
      S_AXI_HP3_ARQOS          : in  std_logic_vector (3 downto 0);
      S_AXI_HP3_AWCACHE        : in  std_logic_vector (3 downto 0);
      S_AXI_HP3_AWLEN          : in  std_logic_vector (3 downto 0);
      S_AXI_HP3_AWQOS          : in  std_logic_vector (3 downto 0);
      S_AXI_HP3_ARID           : in  std_logic_vector (5 downto 0);
      S_AXI_HP3_AWID           : in  std_logic_vector (5 downto 0);
      S_AXI_HP3_WID            : in  std_logic_vector (5 downto 0);
      S_AXI_HP3_WDATA          : in  std_logic_vector (63 downto 0);
      S_AXI_HP3_WSTRB          : in  std_logic_vector (7 downto 0);

      FCLK_CLK0     : out   std_logic;
      FCLK_RESET0_N : out   std_logic;
      MIO           : inout std_logic_vector(C_MIO_PRIMITIVE-1 downto 0);
      DDR_CAS_n     : inout std_logic;
      DDR_CKE       : inout std_logic;
      DDR_Clk_n     : inout std_logic;
      DDR_Clk       : inout std_logic;
      DDR_CS_n      : inout std_logic;
      DDR_DRSTB     : inout std_logic;
      DDR_ODT       : inout std_logic;
      DDR_RAS_n     : inout std_logic;
      DDR_WEB       : inout std_logic;
      DDR_BankAddr  : inout std_logic_vector(2 downto 0);
      DDR_Addr      : inout std_logic_vector(14 downto 0);
      DDR_VRN       : inout std_logic;
      DDR_VRP       : inout std_logic;
      DDR_DM        : inout std_logic_vector(C_DM_WIDTH-1 downto 0);
      DDR_DQ        : inout std_logic_vector(C_DQ_WIDTH-1 downto 0);
      DDR_DQS_n     : inout std_logic_vector(C_DQS_WIDTH-1 downto 0);
      DDR_DQS       : inout std_logic_vector(C_DQS_WIDTH-1 downto 0);
      PS_SRSTB      : inout std_logic;
      PS_CLK        : inout std_logic;
      PS_PORB       : inout std_logic
      );
  end component;

  -- deal with ACP ID widths being less than HP ID widths
  -- intermediate outputs from processors
  signal
    s_axi_acp_in_AR_ID_int, s_axi_acp_in_AW_ID_int, s_axi_acp_in_W_ID_int,
    s_axi_acp_out_B_ID_int, s_axi_acp_out_R_ID_int : std_logic_vector(C_S_AXI_ACP_ID_WIDTH-1 downto 0);

begin

  -- for now we assume 2 nodes and 4 buffers
  s_axi_acp_in_AR_ID_int <= s_axi_acp_in.AR.ID(3 downto 3) & s_axi_acp_in.AR.ID(1 downto 0);
  s_axi_acp_in_AW_ID_int <= s_axi_acp_in.AW.ID(3 downto 3) & s_axi_acp_in.AW.ID(1 downto 0);
  s_axi_acp_in_W_ID_int  <= s_axi_acp_in.W.ID(3 downto 3) & s_axi_acp_in.W.ID(1 downto 0);
  s_axi_acp_out.R.ID     <= "00" & s_axi_acp_out_R_ID_int(2 downto 2) & "0" & s_axi_acp_out_R_ID_int(1 downto 0);
  s_axi_acp_out.B.ID     <= "00" & s_axi_acp_out_B_ID_int(2 downto 2) & "0" & s_axi_acp_out_B_ID_int(1 downto 0);

  -- This instantiation follows the one from the xps-ocpi project, in the file
  -- system_processing_system7_0_wrapper.v
  ps : preset_config_wrapper_processing_system7_0_0  -- For Reference Design use: PZ7030_FMC2_ps7_0
    port map(
      --SDIO0_WP => '0',      -- Use for reference design
      TTC0_WAVE0_OUT      => open,
      TTC0_WAVE1_OUT      => open,
      TTC0_WAVE2_OUT      => open,
      USB0_PORT_INDCTL    => open,
      USB0_VBUS_PWRSELECT => open,
      USB0_VBUS_PWRFAULT  => '0',
      M_AXI_GP0_ARVALID   => m_axi_gp_out(0).AR.VALID,
      M_AXI_GP0_AWVALID   => m_axi_gp_out(0).AW.VALID,
      M_AXI_GP0_BREADY    => m_axi_gp_out(0).B.READY,
      M_AXI_GP0_RREADY    => m_axi_gp_out(0).R.READY,
      M_AXI_GP0_WLAST     => m_axi_gp_out(0).W.LAST,
      M_AXI_GP0_WVALID    => m_axi_gp_out(0).W.VALID,
      M_AXI_GP0_ARID      => m_axi_gp_out(0).AR.ID,
      M_AXI_GP0_AWID      => m_axi_gp_out(0).AW.ID,
      M_AXI_GP0_WID       => m_axi_gp_out(0).W.ID,
      M_AXI_GP0_ARBURST   => m_axi_gp_out(0).AR.BURST,
      M_AXI_GP0_ARLOCK    => m_axi_gp_out(0).AR.LOCK,
      M_AXI_GP0_ARSIZE    => m_axi_gp_out(0).AR.SIZE,
      M_AXI_GP0_AWBURST   => m_axi_gp_out(0).AW.BURST,
      M_AXI_GP0_AWLOCK    => m_axi_gp_out(0).AW.LOCK,
      M_AXI_GP0_AWSIZE    => m_axi_gp_out(0).AW.SIZE,
      M_AXI_GP0_ARPROT    => m_axi_gp_out(0).AR.PROT,
      M_AXI_GP0_AWPROT    => m_axi_gp_out(0).AW.PROT,
      M_AXI_GP0_ARADDR    => m_axi_gp_out(0).AR.ADDR,
      M_AXI_GP0_AWADDR    => m_axi_gp_out(0).AW.ADDR,
      M_AXI_GP0_WDATA     => m_axi_gp_out(0).W.DATA,
      M_AXI_GP0_ARCACHE   => m_axi_gp_out(0).AR.CACHE,
      M_AXI_GP0_ARLEN     => m_axi_gp_out(0).AR.LEN,
      M_AXI_GP0_ARQOS     => open,      -- m_axi_gp_out(0).AR.QOS,
      M_AXI_GP0_AWCACHE   => m_axi_gp_out(0).AW.CACHE,
      M_AXI_GP0_AWLEN     => m_axi_gp_out(0).AW.LEN,
      M_AXI_GP0_AWQOS     => open,      -- m_axi_gp_out(0).AW.QOS,
      M_AXI_GP0_WSTRB     => m_axi_gp_out(0).W.STRB,
      M_AXI_GP0_ACLK      => m_axi_gp_in(0).A.CLK,
      M_AXI_GP0_ARREADY   => m_axi_gp_in(0).AR.READY,
      M_AXI_GP0_AWREADY   => m_axi_gp_in(0).AW.READY,
      M_AXI_GP0_BVALID    => m_axi_gp_in(0).B.VALID,
      M_AXI_GP0_RLAST     => m_axi_gp_in(0).R.LAST,
      M_AXI_GP0_RVALID    => m_axi_gp_in(0).R.VALID,
      M_AXI_GP0_WREADY    => m_axi_gp_in(0).W.READY,
      M_AXI_GP0_BID       => m_axi_gp_in(0).B.ID,
      M_AXI_GP0_RID       => m_axi_gp_in(0).R.ID,
      M_AXI_GP0_BRESP     => m_axi_gp_in(0).B.RESP,
      M_AXI_GP0_RRESP     => m_axi_gp_in(0).R.RESP,
      M_AXI_GP0_RDATA     => m_axi_gp_in(0).R.DATA,

      --S_AXI_ACP_ARESETN => s_axi_acp_in.ARESETN,
      S_AXI_ACP_AWREADY => s_axi_acp_out.AW.READY,
      S_AXI_ACP_ARREADY => s_axi_acp_out.AR.READY,
      S_AXI_ACP_BVALID  => s_axi_acp_out.B.VALID,
      S_AXI_ACP_RLAST   => s_axi_acp_out.R.LAST,
      S_AXI_ACP_RVALID  => s_axi_acp_out.R.VALID,
      S_AXI_ACP_WREADY  => s_axi_acp_out.W.READY,
      S_AXI_ACP_BRESP   => s_axi_acp_out.B.RESP,
      S_AXI_ACP_RRESP   => s_axi_acp_out.R.RESP,
      S_AXI_ACP_BID     => s_axi_acp_out_B_ID_int,
      S_AXI_ACP_RID     => s_axi_acp_out_R_ID_int,
      S_AXI_ACP_RDATA   => s_axi_acp_out.R.DATA,
      S_AXI_ACP_ACLK    => s_axi_acp_in.A.CLK,
      S_AXI_ACP_ARVALID => s_axi_acp_in.AR.VALID,
      S_AXI_ACP_AWVALID => s_axi_acp_in.AW.VALID,
      S_AXI_ACP_BREADY  => s_axi_acp_in.B.READY,
      S_AXI_ACP_RREADY  => s_axi_acp_in.R.READY,
      S_AXI_ACP_WLAST   => s_axi_acp_in.W.LAST,
      S_AXI_ACP_WVALID  => s_axi_acp_in.W.VALID,
      S_AXI_ACP_ARID    => s_axi_acp_in_AR_ID_int,
      S_AXI_ACP_ARPROT  => s_axi_acp_in.AR.PROT,
      S_AXI_ACP_AWID    => s_axi_acp_in_AW_ID_int,
      S_AXI_ACP_AWPROT  => s_axi_acp_in.AW.PROT,
      S_AXI_ACP_WID     => s_axi_acp_in_W_ID_int,
      S_AXI_ACP_ARADDR  => s_axi_acp_in.AR.ADDR,
      S_AXI_ACP_AWADDR  => s_axi_acp_in.AW.ADDR,
      S_AXI_ACP_ARCACHE => s_axi_acp_in.AR.CACHE,
      S_AXI_ACP_ARLEN   => s_axi_acp_in.AR.LEN,
      S_AXI_ACP_ARQOS   => (others => '0'),  -- s_axi_acp_in.AR.QOS,
      S_AXI_ACP_AWCACHE => s_axi_acp_in.AW.CACHE,
      S_AXI_ACP_AWLEN   => s_axi_acp_in.AW.LEN,
      S_AXI_ACP_AWQOS   => (others => '0'),  -- s_axi_acp_in.AW.QOS,
      S_AXI_ACP_ARBURST => s_axi_acp_in.AR.BURST,
      S_AXI_ACP_ARLOCK  => s_axi_acp_in.AR.LOCK,
      S_AXI_ACP_ARSIZE  => s_axi_acp_in.AR.SIZE,
      S_AXI_ACP_AWBURST => s_axi_acp_in.AW.BURST,
      S_AXI_ACP_AWLOCK  => s_axi_acp_in.AW.LOCK,
      S_AXI_ACP_AWSIZE  => s_axi_acp_in.AW.SIZE,
      S_AXI_ACP_ARUSER  => (others => '1'),  -- ones are needed for coherent -- s_axi_acp_in.AR.USER,
      S_AXI_ACP_AWUSER  => (others => '1'),  -- ones are needed for coherent -- s_axi_acp_in.AW.USER,
      S_AXI_ACP_WDATA   => s_axi_acp_in.W.DATA,
      S_AXI_ACP_WSTRB   => s_axi_acp_in.W.STRB,

      --S_AXI_HP0_ARESETN        => s_axi_hp_out(0).A.RESETN,
      S_AXI_HP0_ARREADY        => s_axi_hp_out(0).AR.READY,
      S_AXI_HP0_AWREADY        => s_axi_hp_out(0).AW.READY,
      S_AXI_HP0_BVALID         => s_axi_hp_out(0).B.VALID,
      S_AXI_HP0_RLAST          => s_axi_hp_out(0).R.LAST,
      S_AXI_HP0_RVALID         => s_axi_hp_out(0).R.VALID,
      S_AXI_HP0_WREADY         => s_axi_hp_out(0).W.READY,
      S_AXI_HP0_BRESP          => s_axi_hp_out(0).B.RESP,
      S_AXI_HP0_RRESP          => s_axi_hp_out(0).R.RESP,
      S_AXI_HP0_BID            => s_axi_hp_out(0).B.ID,
      S_AXI_HP0_RID            => s_axi_hp_out(0).R.ID,
      S_AXI_HP0_RDATA          => s_axi_hp_out(0).R.DATA,
      S_AXI_HP0_RCOUNT         => open,  -- s_axi_hp_out(0).R.COUNT,
      S_AXI_HP0_WCOUNT         => open,  -- s_axi_hp_out(0).W.COUNT,
      S_AXI_HP0_RACOUNT        => open,  -- s_axi_hp_out(0).AR.COUNT,
      S_AXI_HP0_WACOUNT        => open,  -- s_axi_hp_out(0).AW.COUNT,
      S_AXI_HP0_ACLK           => s_axi_hp_in(0).A.CLK,
      S_AXI_HP0_ARVALID        => s_axi_hp_in(0).AR.VALID,
      S_AXI_HP0_AWVALID        => s_axi_hp_in(0).AW.VALID,
      S_AXI_HP0_BREADY         => s_axi_hp_in(0).B.READY,
      S_AXI_HP0_RDISSUECAP1_EN => '0',  -- s_axi_hp_in(0).AR.ISSUECAP1_EN,
      S_AXI_HP0_RREADY         => s_axi_hp_in(0).R.READY,
      S_AXI_HP0_WLAST          => s_axi_hp_in(0).W.LAST,
      S_AXI_HP0_WRISSUECAP1_EN => '0',  -- s_axi_hp_in(0).AW.ISSUECAP1_EN,
      S_AXI_HP0_WVALID         => s_axi_hp_in(0).W.VALID,
      S_AXI_HP0_ARBURST        => s_axi_hp_in(0).AR.BURST,
      S_AXI_HP0_ARLOCK         => s_axi_hp_in(0).AR.LOCK,
      S_AXI_HP0_ARSIZE         => s_axi_hp_in(0).AR.SIZE,
      S_AXI_HP0_AWBURST        => s_axi_hp_in(0).AW.BURST,
      S_AXI_HP0_AWLOCK         => s_axi_hp_in(0).AW.LOCK,
      S_AXI_HP0_AWSIZE         => s_axi_hp_in(0).AW.SIZE,
      S_AXI_HP0_ARPROT         => s_axi_hp_in(0).AR.PROT,
      S_AXI_HP0_AWPROT         => s_axi_hp_in(0).AW.PROT,
      S_AXI_HP0_ARADDR         => s_axi_hp_in(0).AR.ADDR,
      S_AXI_HP0_AWADDR         => s_axi_hp_in(0).AW.ADDR,
      S_AXI_HP0_ARCACHE        => s_axi_hp_in(0).AR.CACHE,
      S_AXI_HP0_ARLEN          => s_axi_hp_in(0).AR.LEN,
      S_AXI_HP0_ARQOS          => (others => '0'),  -- s_axi_hp_in(0).AR.QOS,
      S_AXI_HP0_AWCACHE        => s_axi_hp_in(0).AW.CACHE,
      S_AXI_HP0_AWLEN          => s_axi_hp_in(0).AW.LEN,
      S_AXI_HP0_AWQOS          => (others => '0'),  -- s_axi_hp_in(0).AW.QOS,
      S_AXI_HP0_ARID           => s_axi_hp_in(0).AR.ID,
      S_AXI_HP0_AWID           => s_axi_hp_in(0).AW.ID,
      S_AXI_HP0_WID            => s_axi_hp_in(0).W.ID,
      S_AXI_HP0_WDATA          => s_axi_hp_in(0).W.DATA,
      S_AXI_HP0_WSTRB          => s_axi_hp_in(0).W.STRB,

      --S_AXI_HP1_ARESETN        => s_axi_hp_out(1).A.RESETN,
      S_AXI_HP1_ARREADY        => s_axi_hp_out(1).AR.READY,
      S_AXI_HP1_AWREADY        => s_axi_hp_out(1).AW.READY,
      S_AXI_HP1_BVALID         => s_axi_hp_out(1).B.VALID,
      S_AXI_HP1_RLAST          => s_axi_hp_out(1).R.LAST,
      S_AXI_HP1_RVALID         => s_axi_hp_out(1).R.VALID,
      S_AXI_HP1_WREADY         => s_axi_hp_out(1).W.READY,
      S_AXI_HP1_BRESP          => s_axi_hp_out(1).B.RESP,
      S_AXI_HP1_RRESP          => s_axi_hp_out(1).R.RESP,
      S_AXI_HP1_BID            => s_axi_hp_out(1).B.ID,
      S_AXI_HP1_RID            => s_axi_hp_out(1).R.ID,
      S_AXI_HP1_RDATA          => s_axi_hp_out(1).R.DATA,
      S_AXI_HP1_RCOUNT         => open,  -- s_axi_hp_out(1).R.COUNT,
      S_AXI_HP1_WCOUNT         => open,  -- s_axi_hp_out(1).W.COUNT,
      S_AXI_HP1_RACOUNT        => open,  -- s_axi_hp_out(1).AR.COUNT,
      S_AXI_HP1_WACOUNT        => open,  -- s_axi_hp_out(1).AW.COUNT,
      S_AXI_HP1_ACLK           => s_axi_hp_in(1).A.CLK,
      S_AXI_HP1_ARVALID        => s_axi_hp_in(1).AR.VALID,
      S_AXI_HP1_AWVALID        => s_axi_hp_in(1).AW.VALID,
      S_AXI_HP1_BREADY         => s_axi_hp_in(1).B.READY,
      S_AXI_HP1_RDISSUECAP1_EN => '0',  -- s_axi_hp_in(1).AR.ISSUECAP1_EN,
      S_AXI_HP1_RREADY         => s_axi_hp_in(1).R.READY,
      S_AXI_HP1_WLAST          => s_axi_hp_in(1).W.LAST,
      S_AXI_HP1_WRISSUECAP1_EN => '0',  -- s_axi_hp_in(1).AW.ISSUECAP1_EN,
      S_AXI_HP1_WVALID         => s_axi_hp_in(1).W.VALID,
      S_AXI_HP1_ARBURST        => s_axi_hp_in(1).AR.BURST,
      S_AXI_HP1_ARLOCK         => s_axi_hp_in(1).AR.LOCK,
      S_AXI_HP1_ARSIZE         => s_axi_hp_in(1).AR.SIZE,
      S_AXI_HP1_AWBURST        => s_axi_hp_in(1).AW.BURST,
      S_AXI_HP1_AWLOCK         => s_axi_hp_in(1).AW.LOCK,
      S_AXI_HP1_AWSIZE         => s_axi_hp_in(1).AW.SIZE,
      S_AXI_HP1_ARPROT         => s_axi_hp_in(1).AR.PROT,
      S_AXI_HP1_AWPROT         => s_axi_hp_in(1).AW.PROT,
      S_AXI_HP1_ARADDR         => s_axi_hp_in(1).AR.ADDR,
      S_AXI_HP1_AWADDR         => s_axi_hp_in(1).AW.ADDR,
      S_AXI_HP1_ARCACHE        => s_axi_hp_in(1).AR.CACHE,
      S_AXI_HP1_ARLEN          => s_axi_hp_in(1).AR.LEN,
      S_AXI_HP1_ARQOS          => (others => '0'),  -- s_axi_hp_in(1).AR.QOS,
      S_AXI_HP1_AWCACHE        => s_axi_hp_in(1).AW.CACHE,
      S_AXI_HP1_AWLEN          => s_axi_hp_in(1).AW.LEN,
      S_AXI_HP1_AWQOS          => (others => '0'),  -- s_axi_hp_in(1).AW.QOS,
      S_AXI_HP1_ARID           => s_axi_hp_in(1).AR.ID,
      S_AXI_HP1_AWID           => s_axi_hp_in(1).AW.ID,
      S_AXI_HP1_WID            => s_axi_hp_in(1).W.ID,
      S_AXI_HP1_WDATA          => s_axi_hp_in(1).W.DATA,
      S_AXI_HP1_WSTRB          => s_axi_hp_in(1).W.STRB,

      --S_AXI_HP2_ARESETN        => s_axi_hp_out(2).A.RESETN,
      S_AXI_HP2_ARREADY        => s_axi_hp_out(2).AR.READY,
      S_AXI_HP2_AWREADY        => s_axi_hp_out(2).AW.READY,
      S_AXI_HP2_BVALID         => s_axi_hp_out(2).B.VALID,
      S_AXI_HP2_RLAST          => s_axi_hp_out(2).R.LAST,
      S_AXI_HP2_RVALID         => s_axi_hp_out(2).R.VALID,
      S_AXI_HP2_WREADY         => s_axi_hp_out(2).W.READY,
      S_AXI_HP2_BRESP          => s_axi_hp_out(2).B.RESP,
      S_AXI_HP2_RRESP          => s_axi_hp_out(2).R.RESP,
      S_AXI_HP2_BID            => s_axi_hp_out(2).B.ID,
      S_AXI_HP2_RID            => s_axi_hp_out(2).R.ID,
      S_AXI_HP2_RDATA          => s_axi_hp_out(2).R.DATA,
      S_AXI_HP2_RCOUNT         => open,  -- s_axi_hp_out(2).R.COUNT,
      S_AXI_HP2_WCOUNT         => open,  -- s_axi_hp_out(2).W.COUNT,
      S_AXI_HP2_RACOUNT        => open,  -- s_axi_hp_out(2).AR.COUNT,
      S_AXI_HP2_WACOUNT        => open,  -- s_axi_hp_out(2).AW.COUNT,
      S_AXI_HP2_ACLK           => s_axi_hp_in(2).A.CLK,
      S_AXI_HP2_ARVALID        => s_axi_hp_in(2).AR.VALID,
      S_AXI_HP2_AWVALID        => s_axi_hp_in(2).AW.VALID,
      S_AXI_HP2_BREADY         => s_axi_hp_in(2).B.READY,
      S_AXI_HP2_RDISSUECAP1_EN => '0',  -- s_axi_hp_in(2).AR.ISSUECAP1_EN,
      S_AXI_HP2_RREADY         => s_axi_hp_in(2).R.READY,
      S_AXI_HP2_WLAST          => s_axi_hp_in(2).W.LAST,
      S_AXI_HP2_WRISSUECAP1_EN => '0',  -- s_axi_hp_in(2).AW.ISSUECAP1_EN,
      S_AXI_HP2_WVALID         => s_axi_hp_in(2).W.VALID,
      S_AXI_HP2_ARBURST        => s_axi_hp_in(2).AR.BURST,
      S_AXI_HP2_ARLOCK         => s_axi_hp_in(2).AR.LOCK,
      S_AXI_HP2_ARSIZE         => s_axi_hp_in(2).AR.SIZE,
      S_AXI_HP2_AWBURST        => s_axi_hp_in(2).AW.BURST,
      S_AXI_HP2_AWLOCK         => s_axi_hp_in(2).AW.LOCK,
      S_AXI_HP2_AWSIZE         => s_axi_hp_in(2).AW.SIZE,
      S_AXI_HP2_ARPROT         => s_axi_hp_in(2).AR.PROT,
      S_AXI_HP2_AWPROT         => s_axi_hp_in(2).AW.PROT,
      S_AXI_HP2_ARADDR         => s_axi_hp_in(2).AR.ADDR,
      S_AXI_HP2_AWADDR         => s_axi_hp_in(2).AW.ADDR,
      S_AXI_HP2_ARCACHE        => s_axi_hp_in(2).AR.CACHE,
      S_AXI_HP2_ARLEN          => s_axi_hp_in(2).AR.LEN,
      S_AXI_HP2_ARQOS          => (others => '0'),  -- s_axi_hp_in(2).AR.QOS,
      S_AXI_HP2_AWCACHE        => s_axi_hp_in(2).AW.CACHE,
      S_AXI_HP2_AWLEN          => s_axi_hp_in(2).AW.LEN,
      S_AXI_HP2_AWQOS          => (others => '0'),  -- s_axi_hp_in(2).AW.QOS,
      S_AXI_HP2_ARID           => s_axi_hp_in(2).AR.ID,
      S_AXI_HP2_AWID           => s_axi_hp_in(2).AW.ID,
      S_AXI_HP2_WID            => s_axi_hp_in(2).W.ID,
      S_AXI_HP2_WDATA          => s_axi_hp_in(2).W.DATA,
      S_AXI_HP2_WSTRB          => s_axi_hp_in(2).W.STRB,

      --S_AXI_HP3_ARESETN        => s_axi_hp_out(3).A.RESETN,
      S_AXI_HP3_ARREADY        => s_axi_hp_out(3).AR.READY,
      S_AXI_HP3_AWREADY        => s_axi_hp_out(3).AW.READY,
      S_AXI_HP3_BVALID         => s_axi_hp_out(3).B.VALID,
      S_AXI_HP3_RLAST          => s_axi_hp_out(3).R.LAST,
      S_AXI_HP3_RVALID         => s_axi_hp_out(3).R.VALID,
      S_AXI_HP3_WREADY         => s_axi_hp_out(3).W.READY,
      S_AXI_HP3_BRESP          => s_axi_hp_out(3).B.RESP,
      S_AXI_HP3_RRESP          => s_axi_hp_out(3).R.RESP,
      S_AXI_HP3_BID            => s_axi_hp_out(3).B.ID,
      S_AXI_HP3_RID            => s_axi_hp_out(3).R.ID,
      S_AXI_HP3_RDATA          => s_axi_hp_out(3).R.DATA,
      S_AXI_HP3_RCOUNT         => open,  -- s_axi_hp_out(3).R.COUNT,
      S_AXI_HP3_WCOUNT         => open,  -- s_axi_hp_out(3).W.COUNT,
      S_AXI_HP3_RACOUNT        => open,  -- s_axi_hp_out(3).AR.COUNT,
      S_AXI_HP3_WACOUNT        => open,  -- s_axi_hp_out(3).AW.COUNT,
      S_AXI_HP3_ACLK           => s_axi_hp_in(3).A.CLK,
      S_AXI_HP3_ARVALID        => s_axi_hp_in(3).AR.VALID,
      S_AXI_HP3_AWVALID        => s_axi_hp_in(3).AW.VALID,
      S_AXI_HP3_BREADY         => s_axi_hp_in(3).B.READY,
      S_AXI_HP3_RDISSUECAP1_EN => '0',  -- s_axi_hp_in(3).AR.ISSUECAP1_EN,
      S_AXI_HP3_RREADY         => s_axi_hp_in(3).R.READY,
      S_AXI_HP3_WLAST          => s_axi_hp_in(3).W.LAST,
      S_AXI_HP3_WRISSUECAP1_EN => '0',  -- s_axi_hp_in(3).AW.ISSUECAP1_EN,
      S_AXI_HP3_WVALID         => s_axi_hp_in(3).W.VALID,
      S_AXI_HP3_ARBURST        => s_axi_hp_in(3).AR.BURST,
      S_AXI_HP3_ARLOCK         => s_axi_hp_in(3).AR.LOCK,
      S_AXI_HP3_ARSIZE         => s_axi_hp_in(3).AR.SIZE,
      S_AXI_HP3_AWBURST        => s_axi_hp_in(3).AW.BURST,
      S_AXI_HP3_AWLOCK         => s_axi_hp_in(3).AW.LOCK,
      S_AXI_HP3_AWSIZE         => s_axi_hp_in(3).AW.SIZE,
      S_AXI_HP3_ARPROT         => s_axi_hp_in(3).AR.PROT,
      S_AXI_HP3_AWPROT         => s_axi_hp_in(3).AW.PROT,
      S_AXI_HP3_ARADDR         => s_axi_hp_in(3).AR.ADDR,
      S_AXI_HP3_AWADDR         => s_axi_hp_in(3).AW.ADDR,
      S_AXI_HP3_ARCACHE        => s_axi_hp_in(3).AR.CACHE,
      S_AXI_HP3_ARLEN          => s_axi_hp_in(3).AR.LEN,
      S_AXI_HP3_ARQOS          => (others => '0'),  -- s_axi_hp_in(3).AR.QOS,
      S_AXI_HP3_AWCACHE        => s_axi_hp_in(3).AW.CACHE,
      S_AXI_HP3_AWLEN          => s_axi_hp_in(3).AW.LEN,
      S_AXI_HP3_AWQOS          => (others => '0'),  -- s_axi_hp_in(3).AW.QOS,
      S_AXI_HP3_ARID           => s_axi_hp_in(3).AR.ID,
      S_AXI_HP3_AWID           => s_axi_hp_in(3).AW.ID,
      S_AXI_HP3_WID            => s_axi_hp_in(3).W.ID,
      S_AXI_HP3_WDATA          => s_axi_hp_in(3).W.DATA,
      S_AXI_HP3_WSTRB          => s_axi_hp_in(3).W.STRB,

      FCLK_CLK0     => ps_out.FCLK(0),
      FCLK_RESET0_N => ps_out.FCLKRESET_N,
      -- these have no use here since they are hardwired and their I/O configuration
      -- is done in software by programming registers
      MIO           => open,            -- ps_inout.MIO,
      DDR_Clk       => open,            -- ps_inout.DDR_Clk,
      DDR_Clk_n     => open,            -- ps_inout.DDR_Clk_n,
      DDR_CKE       => open,            -- ps_inout.DDR_CKE,
      DDR_CS_n      => open,            -- ps_inout.DDR_CS_n,
      DDR_RAS_n     => open,            -- ps_inout.DDR_RAS_n,
      DDR_CAS_n     => open,            -- ps_inout.DDR_CAS_n,
      DDR_WEB       => open,            -- ps_inout.DDR_WEB,
      DDR_BankAddr  => open,            -- ps_inout.DDR_BankAddr,
      DDR_Addr      => open,            -- ps_inout.DDR_Addr,
      DDR_ODT       => open,            -- ps_inout.DDR_ODT,
      DDR_DRSTB     => open,            -- ps_inout.DDR_DRSTB,
      DDR_DQ        => open,            -- ps_inout.DDR_DQ,
      DDR_DM        => open,            -- ps_inout.DDR_DM,
      DDR_DQS       => open,            -- ps_inout.DDR_DQS,
      DDR_DQS_n     => open,            -- ps_inout.DDR_DQS_n,
      DDR_VRN       => open,            -- ps_inout.DDR_VRN,
      DDR_VRP       => open,            -- ps_inout.DDR_VRP,
      PS_SRSTB      => open,            -- ps_in.PS_SRSTB,
      PS_CLK        => open,            -- ps_in.PS_CLK,
      PS_PORB       => open             -- ps_in.PS_PORB,
      );
end rtl;
